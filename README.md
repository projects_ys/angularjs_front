## Aplicación Front 2 HRI

Sistema de Recursos Humanos de HR Solutions

## Servidor local de desarrollo
El codigo contenido permite ejecutar un servidor local de desarrollo (node.js) que facilita el desarrollo
y despliegue del código de la aplicación. A fin de utilizar el servidor local de desarrollo, se debe 
instalar las dependencias declaradas en el archivo `package.json` ejecutando `npm install` desde la
la raíz de la aplicación.



Ejecutar los siguiente comandos, para cargar dependencias:

```sh
$ npm install
$ bower install

```

Correr servidor: 

```sh

# develoment
$ grunt live

# production
$ grunt server

```

```sh
## Estructura de la aplicación


  .gitignore
  Gruntfile.js
  bower.json
  package.json
  dist/ 
  node_modules/
  bower_components/
  app
  ├── less/
  ├── scripts
  │   ├── vendor/
  │   ├── inspinia/
  │   └── interactive
  │       ├── config/
  │       ├── controllers/
  │       ├── directives/
  │       ├── resources/
  │       └── main.js
  ├── styles
  │   ├── patterns/
  │   ├── plugins/
  │   └── *.css
  ├── views
  │   └── module
  │       └── *.html
  └── index.html
```