/**
 * INSPINIA - Responsive Admin Theme
 *
 */
 (function () {
 	'use strict';
 	angular.module('inspinia', [        
  	'ui.bootstrap'                 // Bootstrap
  ])
 })();