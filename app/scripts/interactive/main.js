(function(){
	'use strict';
	angular.module('Interactive',[
		'inspinia',				// Template
		'ui.router',			// Routing
		'oc.lazyLoad',			// ocLazyLoad
		'ngIdle',				// Idle timer
		'ngSanitize',			// ngSanitize
		'interactive.config',
		'ng-token-auth',
		'ngResource',
		'pdf',
		'summernote',			//Editor de contenidos
		'cgNotify',				//Notificaciones
		'oitozero.ngSweetAlert',//Alertas
		'pascalprecht.translate',
		'psResponsive',
		'angular-ladda',
		'formly',
		'formlyBootstrap',
		'ngMessages', //Mensajes de validacion, Formly
		'angular-tour',
		'ui.select',
		'ui.bootstrap',
		'ngTagsInput',
		'ngDialog',
		'angularMoment',
		'ui.bootstrap.datetimepicker',
		'ui.tree',
		'ngDragDrop',
		'ang-drag-drop',
		'gantt',
    'gantt.sortable',
    'gantt.movable',
    'gantt.drawtask',
    'gantt.tooltips',
    'gantt.bounds',
    'gantt.progress',
    'gantt.table',
    'gantt.tree',
    'gantt.groups',
    'gantt.resizeSensor',
		'nya.bootstrap.select',
		'ng.deviceDetector',
		'ezplus',
		'angucomplete-alt'
	])
	.run(['$window','$location','$rootScope','$state','$auth','$http','HRAPI_CONF','companyData','$translate','sessionUsers','psResponsive','ngDialog','amMoment','$templateCache',function($window,$location,$rootScope,$state,$auth,$http,HRAPI_CONF,companyData,$translate,sessionUsers,psResponsive,ngDialog,amMoment,$templateCache){
	//	$templateCache.put('views/devise/lockscreen.html', 'lockscreen template');
		//Internacionalizcion a español para el modulo Moment
		amMoment.changeLocale('es');
		/**
		* @rootScope Array msg_error
		* @description variable que almacena el listado de errores en login
		**/
		$rootScope.msg_error = [];
		//Crea la Url para peticionar servicios REST
		$rootScope.url_request = function($route){
			return HRAPI_CONF.apiBaseUrl($route);
		};
		//Descarga de Archivos PDF desde una url
		$rootScope.download_file = function (url, name) {
      $http({
          method: 'GET',
          url: url,
          params: { name: name },
          responseType: 'arraybuffer'
      }).success(function (data, status, headers) {
          headers = headers();

          var filename    = name; //headers['x-filename'];
          var contentType = headers['content-type'];

          var linkElement = document.createElement('a');
          try {
              var blob = new Blob([data], { type: contentType });
              var url  = window.URL.createObjectURL(blob);

              linkElement.setAttribute('href', url);
              linkElement.setAttribute("download", filename);

              var clickEvent = new MouseEvent("click", {
                "view": window,
                "bubbles": true,
                "cancelable": false
              });
              linkElement.dispatchEvent(clickEvent);
          } catch (ex) {
              console.log(ex);
          }
      }).error(function (data) {
          console.log(data);
      });
    };
		//Resolucion promesa de los datos de compañia
		companyData.then(function(data) {
			// add background_lockscreen
			data.company.background_lockscreen.url="styles/patterns/background_lockscreen_hrs.jpg";
			$rootScope.$company = data.company;
		},function(error){
			console.log(error);
		});
		// LocalStorageServices
		$rootScope.data_login_user=sessionUsers.getUser;
		//Responsive
		$rootScope.responsive = psResponsive;
		//State
		$rootScope.$state = $state;


		/**	Funciones de session ng-token auth **/
		$rootScope.$on('auth:login-success', function(ev, user) {
			$rootScope.$user = user;
			sessionUsers.addUser({
				email: user.email,
				name: user.employee.short_name,
				company_id: user.company_id,
				employee_id: user.employee_id,
				photo: user.employee.image.url
			});
			$rootScope.data_login_user=sessionUsers.getUser();
		});
		$rootScope.$on('auth:validation-success', function(ev, user) {
			$rootScope.$user = user;
		});
		$rootScope.$on('auth:invalid', function(ev, reason) {
			$state.transitionTo('devise.login');
		});
		$rootScope.$on('auth:validation-error', function(ev, reason) {
			$state.transitionTo('devise.login');
		});
		$rootScope.$on('auth:login-error', function(ev, reason) {
			$rootScope.msg_error = [];
			$rootScope.msg_error.push(reason.errors[0]);
		});
		$rootScope.$on('auth:logout-error', function(ev, reason) {
			$state.transitionTo('devise.login');
		});
		$rootScope.$on('auth:session-expired', function(ev) {
			$rootScope.data_login_user=sessionUsers.getUser();
			$state.transitionTo('lockscreen');
		});
		//verifica estado de coneccion
		$rootScope.online = navigator.onLine;
		$window.addEventListener("offline", function () {
			$rootScope.$apply(function() {
				$rootScope.online = false;
				$rootScope.data_login_user = sessionUsers.getUser();
				$state.transitionTo('lockscreen');
			});
		}, false);
		$window.addEventListener("online", function () {
			$rootScope.$apply(function() {
				$rootScope.online = true;
			});
		}, false);
		$rootScope.$on('$routeChangeStart', function (event, next,post) {

		});
	}]);
})();
