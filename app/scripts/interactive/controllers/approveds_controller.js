(function(){
  'use strict';
  angular.module('Interactive')
  .controller('ApprovedsController',['$scope','$http','$state','$filter','TYPES_REQUESTS','swalService','requestEvents','Request',function($scope,$http,$state,$filter,TYPES_REQUESTS,swalService,requestEvents,Request){
    $scope.model_approved      = {};

    /**Gantt Settings*/
    $scope.headersFormats = {
      'year': 'YY',
      'quarter': '[Q]Q YY',
      month: 'MMMM YYYY',
      day: 'D',
      hour: 'H',
      minute:'HH:mm'
    };
    $scope.gantt = {
      header : ['month', 'day'],
      current_date: new Date(),
      show_side:'false',
      headers_formats: {
        'year': 'YY',
        'quarter': '[Q]Q YY',
        month: 'MMMM YYYY',
        day: 'D',
        hour: 'H',
        minute:'HH:mm'
      },
      column_width:'viewScale == "day" ? 50 : 20',
      tooltips:{
        content: '<strong>{{task.model.name}}</strong></br>' +
        '<small>' +
        '{{task.isMilestone() === true && getFromLabel() || getFromLabel() + \' - \' + getToLabel()}}' +
        '</small>',date_format:'DD MMM  YY'
      }
    };
    $scope.header = ['month', 'day'];
    $scope.contentTool = '<strong>{{task.model.name}}</strong></br>' +
      '<small>' +
      '{{task.isMilestone() === true && getFromLabel() || getFromLabel() + \' - \' + getToLabel()}}' +
      '</small>';
    $scope.formattooltips = 'DD MMM  YY';
    $scope.getToday = new Date();
    /**End Gantt Settings */

    /**
    * @param String tipo
    * @return Boolean
    * @description Funcion que valida si el tipo de vacaciones que ingresa como parametro es de vacaciones, se me retorna un true.
    **/
    $scope.ischeckVaca = function (tipo) {
      var arrayRequest = ['VACA_001','VACA_002'];
      var indexval     = arrayRequest.indexOf(tipo);
      return indexval >= 0;
    }

    /**
    * @param String status
    **/
    $scope.approved_request = function (status) {
      $scope.model_approved.status_id = (status == 'approved') ? TYPES_REQUESTS.PENDING_APPROVE_RH : TYPES_REQUESTS.CANCEL_APPROVE_BOSS;
      if(status == 'denegate'){
        swalService.custonConfirm('APPROVEDS.DENEGATE.TITLE','APPROVEDS.DENEGATE.MESSAGE','warning','BTN_YES','BTN_CANCEL',
          function () { // en caso confirm is positive true
            swalService.sampleMessage('APPROVEDS.CANCEL.TITLE','APPROVEDS.CANCEL.MESSAGE','info');
          }, 
          function () { // en caso cancel confirm  false 
            $scope.save_approved($scope.model_approved);
          }
        );
      }else{
        $scope.save_approved($scope.model_approved);
      }
    }

    $scope.save_approved = function (params) {
      var url = $scope.url_request("/approvals/"+$scope.request_id);
      $http.put(url, params)
      .then(function(response) {
        $scope.response_approved = response.data;
        swalService.sampleMessage('REQUESTS.TITLE','APPROVEDS.SUCCESS.MESSAGE','success');
        $state.transitionTo("main.approved.index");
      }, function(error) {
        if (error.status === '400') {
          swalService.error400(error.status, error.data);
        }else{
          swalService.error400(error.status, error.data);
        }
      });
    }

    $scope.request_key_state = function(general_state, short_state){
      var state = 1;
      switch(general_state){
        case TYPES_REQUESTS.PENDING_APPROVE_BOSS:
        state = 1;
        break;
        case TYPES_REQUESTS.CANCEL_APPROVE_BOSS:
        state = 2;
        break;
        case TYPES_REQUESTS.PENDING_APPROVE_RH:
        state = 2;
        break;
        case TYPES_REQUESTS.CANCEL_APPROVE_RH:
        state = 3;
        break;
        case TYPES_REQUESTS.APPROVE_RH:
        state = 3;
        break;
      }
      return short_state === state ? 'navy-bg' : 'gray-bg';
    }

    $scope.icon_states = function (state) {
      return requestEvents.request_icon_states(state);
    }

    $scope.btn_states = function (state) {
      return requestEvents.request_btn_states(state);
    };

    $scope.fields = [
     {
       key: 'comment_first_approver',
       type: 'textarea',
       templateOptions: {
         label: 'Observaciones',
         options: $scope.observations_request,
         required: false
       }
     }
   ];
   
  }]);
})();
