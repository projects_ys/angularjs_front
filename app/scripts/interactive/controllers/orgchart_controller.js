(function(){
  'use strict';
  angular.module('Interactive')
  .controller('orgchartController',['$scope','$http','$q','PagerService','organigram','swalService','$timeout',function($scope,$http,$q,PagerService,organigram,swalService,$timeout){

    $scope.datasource={};
    $scope.employee_selected="";
    $scope.show_error=false;
    var chartContainer =  $('#chart-container');
    var params ={'page':'1','pernr':'','pageSize':'5','naveg':[{'page':'1','status':'default'}]};
    var direction ='t2b';

    initOrgchart(params);
    function initOrgchart (params) {
      $scope.show_error=false;
      chartContainer.find('.orgchart:visible').addClass('hidden');
      chartContainer.find('.error_500').addClass('hidden');
      chartContainer.append('<div class="loading-fa sk-spinner sk-spinner-three-bounce" style="position: fixed;top: 0;left: 0;right: 0;bottom: 0;margin: auto;z-index: 1100;"><div class="sk-bounce1"></div> <div class="sk-bounce2"></div><div class="sk-bounce3"></div></div>');
      var url = "/organization_chart/";
      if (params.naveg[params.naveg.length-1].status === 'up') {
        params.naveg.pop(params.naveg.pop());
        if(params.naveg.length > 0){
          params.page  = params.naveg[params.naveg.length-1].page;
        }else{
          params.page  = 1;
        }

      }
      if (params.pernr !== '') {
        url = url +"?page="+params.page+"&pernr="+params.pernr;
      }
      organigram($scope.url_request(url)).then(function (response) {
        chartContainer.find('.loading-fa').addClass('hidden');
        $scope.datasource = formateClassDataSoruce(response.data);
        chartContainer.orgchart({
          'chartClass': 'clasnode',
          'data' :   $scope.datasource,
          'nodeId': 'pernr',
          'nodeContent': 'posicion',
          'nodeTitle':"short_name",
          'pan': false,
          'zoom': false,
          'direction':direction,
          'createNode': function($node, data,routeNode) {
            createNodeData($node, data, routeNode);
          }
        });
      },function (error) {
        chartContainer.find('.loading-fa').addClass('hidden');
        $scope.show_error=true;
      });

    };
    var  createNodeData = function ($node, data, routeNode) {
      var secondMenuIcon = $('<i>', {
        'class': 'fa fa-circle-o-notch  second-menu-icon',
        click: function() {
          $(this).siblings('.second-menu').toggle();
        }
      });
      var secondMenu = htmlImageContent(data);
      $node.append(secondMenuIcon).append(secondMenu);
      if ($node.is('.drill-down')) {
        var drillDownIcon = $('<i >', {
          'class': 'fa fa-arrow-circle-down drill-icon poss-down" ',
          'click': function() {
            $node.append("<i class='fa fa-refresh fa-spin spinner' style='z-index:2'></i>");
            params.naveg.push({'page':'1','status':'down'});
            params.page  = 1;
            params.pernr = data.pernr;
            initOrgchart(params);
          }
        });
        $node.append(drillDownIcon);
      }
      if ($node.is('.drill-up')) {
        var drillUpIcon = $('<i >', {
          'class': 'fa fa-arrow-circle-up drill-icon poss-up',
          'click': function() {
            $node.append("<i class='fa fa-circle-o-notch fa-spin spinner' style='z-index:2'></i>");
            params.naveg.push({'page':params.page,'status':'up'});
            params.pernr = data.pernr;
            initOrgchart(params);
          }
        });
        $node.append(drillUpIcon);
      }
      if ($node.is('.drill-left')){
        var drillUpIcon = $('<i>', {
          'class': 'fa fa-arrow-circle-left fa-5x drill-icon poss-left',
          'click': function() {
            $node.append("<i class='fa fa-circle-o-notch fa-spin spinner' style='z-index:2'></i>");
            if (params.page !== 1 ) {
              params.naveg[params.naveg.length-1].page = parseInt(params.naveg[params.naveg.length-1].page )-1;
              params.page  = data.page-1;
            }
            params.pernr = data.pernr_parent;
            initOrgchart(params);
          }
        });
        $node.append(drillUpIcon);

      }
      if ($node.is('.drill-right')){
        var drillUpIcon = $('<i>', {
          'class': 'fa fa-arrow-circle-right fa-5x drill-icon poss-right',
          'click': function() {
            $node.append("<i class='fa fa-circle-o-notch fa-spin spinner' style='z-index:2'></i>");

            if(params.naveg.length > 0){
              params.naveg[params.naveg.length-1].page = parseInt(params.naveg[params.naveg.length-1].page )+1;
            }else{
              params.naveg.push({'page':'2','status':'right'});
            }

            params.page  = parseInt(data.page)+1;
            params.pernr = data.pernr_parent;
            initOrgchart(params);
          }
        });
        $node.append(drillUpIcon);
      }
    };
    var htmlImageContent = function (data) {
      var html="<div class='second-menu'> <div class='row'>"+
      "<div class='datacontent text-center'>"+
      "<div class='text-center' style='padding-top: 0px'>"+
      "<img alt='cargando..' class='avatar text-center' src='"+data.image.url+"' >"+
      "</div>"+
      "<h6 style='margin-top: 5px;font-weight: bold;margin-bottom: 2px;font-size: 9px;text-transform: capitalize;color: black;'>"+data.short_name+"</h6>"+
      "<div style='width:80%;text-align: center;margin: 0 auto;'><h6 style='font-size: 8px;margin-top: 2px;font-weight: bold;margin-bottom: 2px;'>"+data.posicion+"</h6></div>"+
      "<div style='width:70%;text-align: center;margin: 0 auto;'><marquee behavior='scroll' SCROLLDELAY='150' direction='left' style='font-size: 10px;text-transform: lowercase;color: blue;'>"+data.email+"</marquee></div>"+
      "</div>"+
      "</div></div>";
      return html;
    };

    function formateClassDataSoruce(datasource) {
      if (datasource.relationship === '100' || datasource.relationship === '111' || datasource.relationship === '101' ) {
        datasource.className="drill-up";
      }
      if (datasource.children.length !== 0) {
        if (datasource.children[0].children !== undefined ) {
          if (datasource.children[0].children.length >0) {
            if (datasource.children[0].children[0].page > "1") {
              datasource.children[0].children[0].className="drill-left";
            }
            var Pager = PagerService.GetPager(datasource.children[0].total_children, params.page, params.pageSize);
            //console.log("Pager.totalPages:"+Pager.totalPages+"datasource.children[0].total_children mayor "+datasource.children[0].total_children +" a Pager.pageSize "+Pager.pageSize);
            if((params.page < Pager.totalPages) && (datasource.children[0].total_children > Pager.pageSize) ){
              datasource.children[0].children[ datasource.children[0].children.length-1].className="drill-right";
            }
            angular.forEach(datasource.children[0].children, function(value, key) {
              if (value.relationship === '111' || value.relationship === '101' || value.relationship === '001') {
                value.className="drill-down";
              }
            });
          }else{
            if (datasource.children[0].total_children > 0) {
              if (datasource.children[0].relationship === '111' || datasource.children[0].relationship === '101' || datasource.children[0].relationship === '001' ) {
                datasource.children[0].className="drill-down";
              }
            }

          }
        }else{
          angular.forEach(datasource.children, function(value, key) {
            if (value.relationship === '111' || value.relationship === '101' || value.relationship === '001') {
              value.className="drill-down";
            }
          });
          var Pager = PagerService.GetPager(datasource.total_children, params.page, params.pageSize);
          //console.log("Pager.totalPages:"+Pager.totalPages+" params.page "+params.page);
          if((params.page < Pager.totalPages) &&  (params.page >= 1) ){
            datasource.children[ datasource.children.length-1].className="drill-right";
          }
          if(params.page > 1 ){
            datasource.children[0].className="drill-left";
          }
        }
      }
      return datasource;
    };
    $scope.searchAPI = function(userInputString, timeoutPromise) {
      var url= $scope.url_request('/organization_chart/search?search='+userInputString);
      return $http.get(url);
    }


    $scope.selectObjet = function(selected) {
      if (selected) {
        $scope.employee_selected=selected.originalObject.pernr;
        var pernr = selected.originalObject.pernr;
        params ={'page':'1','pernr':pernr,'pageSize':'5','naveg':[{'page':'1','status':'default'}]};
        initOrgchart(params);
      }
    };
    $scope.clickSearch = function() {
        if($scope.employee_selected !==""){
          params ={'page':'1','pernr':$scope.employee_selected,'pageSize':'5','naveg':[{'page':'1','status':'default'}]};
          initOrgchart(params);
        }
    };
    $scope.restar = function () {
      params.pernr="";
      initOrgchart(params);
    }

  }]);
})();
