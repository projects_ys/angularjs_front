(function(){
  'use strict';
  angular.module('Interactive')
  .controller('ManagerServiceController',['$scope',function( $scope){
    //datos de perfil
		$scope.profile          = {};
    // ruta de sortTable
    $scope.route            = '';
    // activa reportes / Movil & desktop
    $scope.viewReportMovil=true;

      // muestra los reportes del empleado / segun la resolucion
    $scope.show_report = function(url, title){
      if($scope.responsive('medium') || $scope.responsive('small')
      || $scope.responsive('tiny') ){
      $scope.viewReportMovil=false;
      }
      $scope.report_title = title;
      $scope.route        = url+"/"+$scope.profile.id;
    };
    // hiden/show reporte de empleado (en Movil)
    $scope.changeViewReportMovil= function () {
       $scope.viewReportMovil=!$scope.viewReportMovil;
    };

    $scope.get_profile = function(employee){
			$scope.profile = employee;
      if($scope.profile.id === undefined){
        $scope.route = ''
      }
    };

    $scope.is_vacant = function(employee){
      if (employee.id !== undefined) {
        return employee.first_name === 'vacante';
      }
      return true;
    };

    $scope.$watch('profile', function (newValue, oldValue, scope) {
      //cada ves que cambia el profile, se restituye el reporte
     $scope.viewReportMovil=true;
    });
	}]);
})();
