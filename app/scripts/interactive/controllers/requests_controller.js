(function(){
  'use strict';
  angular.module('Interactive')
  .controller('RequestsController',['$scope','$state','TYPES_REQUESTS','swalService','requestEvents','Request','$filter','requestForms',function($scope,$state,TYPES_REQUESTS,swalService,requestEvents,Request,$filter,requestForms){
    //Index Variables//
    $scope.filter                = {};
    $scope.filter.type           = 'all';
    $scope.model_form            = {};
    $scope.forms                 = [];

    $scope.model_form.date_begin = '';
    $scope.model_form.date_end   = '';
    $scope.pendind_aprove_boss   = TYPES_REQUESTS.PENDING_APPROVE_BOSS;
    //FileUpload_Resquest
    $scope.file                  = {};
    $scope.fileName              = "file_support";


    $scope.select_filter = function(){
      switch($scope.filter.type){
        case 'all':
        $scope.requests = $scope.requests_slopes.concat($scope.request_managed);
        break;
        case 'slopes':
        $scope.requests = $scope.requests_slopes;
        break;
        case 'managed':
        $scope.requests = $scope.request_managed;
        break;
      }
    }

    $scope.create_route = function(action, value){
      switch(action){
        case 'create': return 'main.request.create';
      }
    }

    $scope.exists_data = function(data){
      if (data !== undefined && data !== null) {
        return data.length !== 0;
      }
      return false;
    }

    /*** Show methods ***/
    $scope.request_key_state = function(general_state, short_state){
      var state = 1;
      switch(general_state){
        case TYPES_REQUESTS.PENDING_APPROVE_BOSS:
        state = 1;
        break;
        case TYPES_REQUESTS.CANCEL_APPROVE_BOSS:
        state = 2;
        break;
        case TYPES_REQUESTS.PENDING_APPROVE_RH:
        state = 2;
        break;
        case TYPES_REQUESTS.CANCEL_APPROVE_RH:
        state = 3;
        break;
        case TYPES_REQUESTS.APPROVE_RH:
        state = 3;
        break;
      }
      return short_state === state ? 'navy-bg' : 'gray-bg';
    }

    /**
    * @param Date date
    * @param String type ['date' || 'time']
    * @return String
    *
    * @description Retorna cadena formateada con el tipo de data que se envia por parametro
    **/
    $scope.date_format = function(date, type){
      switch(type){
        case 'date': return $filter('date')(date, 'dd/MM/yyyy');
        case 'time': return $filter('date')(date, 'HH:mm');
        default: return $filter('date')(date, 'dd/MM/yyyy HH:mm');
      }
    }

    $scope.days_validate = function(days){
      return parseInt(days) !== 0;
    }

    /**
    * @param Date date
    * @param String type ['date' || 'time']
    * @return boolean
    *
    * @description
    **/
    $scope.date_valid = function(date, type){
      switch(type){
        case 'date': return $filter('date')(date, 'dd') !== '00';
        case 'time': return $filter('date')(date, 'HH') !== '00';
      }
      return false;
    }

    $scope.icon_states = function (state) {
      return requestEvents.request_icon_states(state);
    }

    $scope.btn_states = function (state) {
      return requestEvents.request_btn_states(state);
    }

    $scope.get_type_selected = function(type_id){
      return $filter('filter')($scope.type_request, {value: type_id})[0];
    }

    $scope.delete_request = function(idDelete){
      return requestEvents.delete_request(idDelete);
    }

    $scope.create = function(form){
      if (form.$valid) {
        var resourse = null;
        $scope.model_form.types_requests_record_id = $scope.model_request.type_id.value;
        if ($scope.model_form.file_support === undefined || $scope.model_form.file_support === null) {
          resourse = create_request($scope.model_form);
        }else { // solicitud con Adjunto
          resourse = create_request_form($scope.model_form, $scope.fileName, $scope.file);
        }

        resourse.$promise.then(function (response) {
          swalService.sampleMessage('REQUESTS.TITLE', 'REQUESTS.SAVE.MESSAGE', 'success');
          $state.transitionTo('main.request.index');
        },function (error) {
          //$rootScope.errors = error.data;
          swalService.error400(error.status, error.data);
        });
      }else{
        return false;
      }
    };

    /**
     * @param model_form => Json Object
     * @return Request
     **/
    var create_request = function(model_form){
      return Request.save(model_form);
    };

    /**
     * @param model_form => Json Object
     * @param nameFile => String
     * @param file =>
     * @return Request
     **/
    var create_request_form = function(model_form, nameFile, file){
      var formData = new FormData();
      angular.forEach(model_form, function(value, key){
        if (nameFile == key) {
          formData.append(key,file);
        }else {
          formData.append(key,value);
        }
      });
      return Request.save_formData(formData);
    };

    $scope.$watch('model_request.type_id.value', function(newVal, oldVal){
      $scope.request_fields = []
      if(newVal !== undefined){
        $scope.model_form = {};
        $scope.model_form.available_contingent = $filter('filter')($scope.types, {form: newVal})[0].available_contingent;
          $scope.request_fields = requestForms.get_form(newVal, $scope.model_form.available_contingent);
          $scope.total_fields   = $scope.request_fields.length;
          console.log("Contingente: "+$scope.model_form.available_contingent);
      }
    });
    // fileUplad_Request
    //escucha el evento de selecciona Archivo
    $scope.unlisten = $scope.$on('fileToUpload', function(event, arg) {
      $scope.file  = arg;
    });
    $scope.$on('$destroy', $scope.unlisten);

  }]);
})();
