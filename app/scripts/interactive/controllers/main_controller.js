(function(){
  'use strict';
  angular.module('Interactive')
  .controller('mainController',['$scope', '$auth', '$state',function($scope, $auth, $state){
    $scope.logout = function(){
    	$auth.signOut()
	    .then(function(resp){
	    	$state.go('devise.login');
	    })
	    .catch(function(resp){
	    	console.log(resp);
	    	$state.go('devise.login');
	    });
    };
    $scope.minimalize = function () {
     if ($('body').hasClass('body-small')) {
          $("body").toggleClass("mini-navbar");
            $('#side-menu').hide();
              setTimeout(
                function () {
                  $('#side-menu').fadeIn(400);
                }, 200);
      }
    }

	}]);
})();
