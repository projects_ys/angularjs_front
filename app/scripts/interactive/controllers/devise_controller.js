(function(){
  'use strict';

  angular.module('Interactive')
  .controller('deviseController',['$scope', '$auth', '$state','$timeout','sessionUsers','$translate',function($scope, $auth, $state,$timeout,sessionUsers,$translate){
    $scope.view_error        = false;
    $scope.credentials       = {};
    $scope.data_login_user   = sessionUsers.getUser();
    $scope.credentials.email = $scope.data_login_user.email;
    /**
    * @var $scope.credentials.login
    * @description varible para dar soporte al campo del formulario en produccion
    **/
    $scope.credentials.login = $scope.data_login_user.email;
    $scope.loading11         = false;
    $scope.sendingLoging     = false;

    var field_password = {
      key: 'password',
      type: 'password_login_input',
      templateOptions: {
        type: 'password',
        placeholder: '{{"LOGIN.INPUT_PASS " | translate}}',
        required: true
      }
    };

    /// String @type: (password | hidden)
    var get_field_login = function(type){
      return {
        key: 'email',
        type: 'email_login_input',
        templateOptions: {
          type: type,
          placeholder: '{{"LOGIN.INPUT_EMAIL " | translate}}',
          required: true
        }
      };
    }

    $scope.fields_login       = [get_field_login('email'), field_password];
    $scope.fields_forgot_pass = [get_field_login('email')];
    $scope.fields_lockscreen  = [get_field_login('hidden'),field_password];

    $scope.login = function(form) {
      if (form.$valid) {
        $scope.view_error = false;
        $scope.loading11         = true;
        $scope.credentials.login = $scope.credentials.email;
        $auth.submitLogin($scope.credentials)
        .then(function(resp) {
          $state.go('main.dashboard');
        })
        .catch(function(resp){
          $scope.view_error = true;
          $scope.loading11  = false;
        });
      }
    };
    $scope.requestPassword = function(){
      $auth.requestPasswordReset({
        email:$scope.credentials.email
      })
      .then(function(resp) {
        $scope.typemsg = "info";
        $scope.view_error = true;
        $scope.msg_response = resp.data.message;
        $timeout(function () {
          $scope.view_error = false;
          $scope.credentials = {};
          $state.go('devise.login');
        }, 2000);
      })
      .catch(function(resp) {
        $scope.typemsg = "danger";
        $scope.view_error = true;
        $scope.msg_response = resp.data.errors[0];
        $timeout(function () {
          $scope.view_error = false;
        }, 2000);
      });

    }

  $scope.resetPassword = function() {
    $auth.updatePassword({
      'password'              : $scope.credentials.password,
      'password_confirmation' : $scope.credentials.password_confirmation,
      'reset_password_token'  : $state.params.token
    })
    .then(function(resp) {
      $scope.typemsg  = "success";
      $scope.msg_response = resp.data.data.message;
      $scope.view_error = true;
      $timeout(function () {
        $scope.view_error = false;
        $scope.credentials = {};
        $state.go('devise.login');
      }, 2000);
    })
    .catch(function(resp) {
      $scope.typemsg = "danger";
      if(resp.data.errors[0] != undefined){
        $scope.msg_response = resp.data.errors[0];
      }else{
        $scope.msg_response = resp.errors.full_messages;
      }
      $scope.view_error = true;
      $timeout(function () {
        $scope.view_error = false;
      }, 2000);
    });
  };

}]);
})();
