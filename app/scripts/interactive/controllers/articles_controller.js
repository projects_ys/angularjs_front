(function(){
  'use strict';
  angular.module('Interactive')
  .controller('ArticlesController',[ '$scope','Article','$state','swalService','ipCookie','$translate',function( $scope, Article, $state,swalService,ipCookie,$translate){
    //@scope boolean Variable para activar y desactivar boton de envio de formulario
    $scope.sending = false;
    //Formly Edit/Create
    $scope.fields = [
      {
        key: 'publicado',
        type:"onoffswitch",
        model:$scope.model_article,
        className: '',
        templateOptions: {
          label: $translate.instant('MANAGEMENT_CONTENT.FORM.ONOFF')
        },
      },
      {
        key: 'titulo',
        type:"text_input",
        model:$scope.model_article,
        className: '',
        templateOptions: {
          label: $translate.instant('MANAGEMENT_CONTENT.FORM.TITLE'),
          placeholder:$translate.instant('MANAGEMENT_CONTENT.FORM.TITLE'),
          required: true,
          maxlength: 50,
          minlength: 5

        }
      },
      {
        key: 'categoria',
        type: 'grouped_select',
        model:$scope.model_article,
        className: '',
        templateOptions: {
          label: $translate.instant('MANAGEMENT_CONTENT.FORM.CATEGORY'),
          required: true,
          options: $scope.categoriesContent,

        }
      },
      {
        key: 'ini',
        type: 'textarea_input',
        model:$scope.model_article,
        className: '',
        templateOptions: {
          label: $translate.instant('MANAGEMENT_CONTENT.FORM.RESUMEN'),
          placeholder:  $translate.instant('MANAGEMENT_CONTENT.FORM.RESUMEN_PLACEHOLDER'),
          required: true,
          maxlength: 200,
          minlength: 10
        }
      },
      {
        key: 'cuerpo',
        type: 'summernote_input',
        model:$scope.model_article,
        className: '',
        templateOptions: {
          label: $translate.instant('MANAGEMENT_CONTENT.FORM.CONTENT'),
          required: true
        }
      }
    ];
    // eliminar articulos : ID
    $scope.deleteArticle = function(idDelete){
      swalService.custonConfirm('ARTICLES.DELETE.TITLE','DELETE_MESSAGE',"warning",'DELETE_CANCEL_BTN_TEXT','DELETE_CONFIRM_BTN_TEXT',
      function () {
        // en caso  confirm is positive true
          Article.remove({id:idDelete},function(response){
            swalService.sampleMessage('ARTICLES.TITLE','DELETE_CONFIRM_MESSAGE','success');
            $state.go('main.content.all');
            if ($state.current.name === 'main.content.all') {
              $state.reload();
            }
          },function (error) {
            swalService.error400(error.status,error.data);
          }
        );
      },
      function () {
        // en caso  cancel confirm  false
        swalService.sampleMessage('CANCEL_TITLE','ARTICLES.CANCEL.MESSAGE','info');
      }
    );
  };

  $scope.saveContent = function(form,model){
    //size in Megabytes
    var size = "";
    var resourse=null;
    var messageSwal ="";
    if (form.$valid) {
      if (model.cuerpo !== undefined) {
        size = (model.cuerpo.length / 1024) / 1024;
      }

        if (size <= 5) {
          model.category = model.categoria.category;
          if(model.publicado === undefined){
            model.publicado ='false';
          }
          $scope.sending = true;
          if (model.id === undefined) {
            //CREATE ARTICLE
            resourse =Article.save(model);
            messageSwal = "CREATE_MESSAGE";
          }else{
            //UPDATE ARTICLE
            resourse =Article.update(model);
            messageSwal = "UPDATE_MESSAGE";
          }
          resourse.$promise.then(function (response) {
            $scope.sending=false;
            $scope.$user.employee.has_articles=true;
            swalService.sampleMessage('ARTICLES.TITLE',messageSwal,'info');
            $state.transitionTo('main.content.view', {articleId: response.id });
          },function (error) {
            $scope.sending=false;
            swalService.error400(error.status,error.data);
          }
        );
      }else{
        swalService.sampleMessage('ARTICLES.TITLE','ARTICLES.ERROR.SIZE_MAX','error');
      }
    }
  };

  //  pasos del tour
  $scope.currentStep = ipCookie/('myTour') || 0;
  // save cookie after each step
  $scope.stepComplete = function() {
    ipCookie('myTour', $scope.currentStep, { expires: 3000 });
  };

}]);
})();
