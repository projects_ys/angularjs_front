(function(){
  'use strict';
  angular.module('Interactive')
  .controller('localDataSourceController',['$scope', '$auth', '$state',function($scope, $auth, $state){
    var datasource = {
      'name': 'Lao Lao',
      'title': 'general manager',
      'children': [
        { 'name': 'Bo Miao', 'title': 'department manager' },
        { 'name': 'Su Miao', 'title': 'department manager',
        'children': [
          { 'name': 'Tie Hua', 'title': 'senior engineer' },
          { 'name': 'Hei Hei', 'title': 'senior engineer' }
        ]
      },
      { 'name': 'Hong Miao', 'title': 'department manager' },
      { 'name': 'Chun Miao', 'title': 'department manager' }
    ]
  };

  $('#chart-container').orgchart({
  'data' : datasource,
  'depth': 3,
  'nodeContent': 'title',
  'pan': true,
  'zoom': true,
  'depth': 3500,
  'nodeID': 'id',
  'createNode': function($node, data) {
    var secondMenuIcon = $('<i>', {
      'class': 'fa fa-info-circle second-menu-icon',
      click: function() {
        $(this).siblings('.second-menu').toggle();
      }
    });
    var secondMenu = '<div class="second-menu"><img class="avatar"  src="http://www.webconsultas.com/sites/default/files/styles/encabezado_articulo/public/articulos/perfil-resilencia.jpg?itok=iQzjOtzd"></div>';
    $node.append(secondMenuIcon).append(secondMenu);
  }
});

}]);
})();
