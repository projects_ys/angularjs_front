(function(){
	'use strict';
	angular.module('Interactive')
	.controller('dashboardController',['$scope','$state','$http','HRAPI_CONF','$filter','favoriteEmployees','$translate','deviceDetector',function($scope, $state,$http,HRAPI_CONF,$filter,favoriteEmployees,$translate,deviceDetector){
		$scope.search_user		 = [];
		$scope.search_copy 		 = "";
		$scope.showSpinner		 = false;
		$scope.style_favorites = {height:"0px"};
		$scope.favorites 			 = {favorites:[],employees_remaining:[]};
		$scope.padding_class   = deviceDetector.browser === 'ie' || deviceDetector.browser === 'ms-edge' ? 'padding-users-ie' : 'no-padding';

		$scope.vacations_label 							 = $translate.instant('DASHBOARD.VACATIONS.LABEL');
		$scope.vacations_title 							 = $translate.instant('DASHBOARD.VACATIONS.TITLE');
		$scope.vacations_help  							 = $translate.instant('DASHBOARD.VACATIONS.HELP');
		$scope.severances_label 						 = $translate.instant('DASHBOARD.SEVERANCES.LABEL');
		$scope.severances_title 						 = $translate.instant('DASHBOARD.SEVERANCES.TITLE');
		$scope.severances_help  						 = $translate.instant('DASHBOARD.SEVERANCES.HELP');
		$scope.severance_interest_label 		 = $translate.instant('DASHBOARD.SEVERANCE_INTERESTS.LABEL');
		$scope.severance_interest_title 		 = $translate.instant('DASHBOARD.SEVERANCE_INTERESTS.TITLE');
		$scope.severance_interest_help  		 = $translate.instant('DASHBOARD.SEVERANCE_INTERESTS.HELP');
		$scope.income_and_deductions_label_1 = $translate.instant('DASHBOARD.INCOME_AND_DEDUCTIONS.LABEL_1');
		$scope.income_and_deductions_label_2 = $translate.instant('DASHBOARD.INCOME_AND_DEDUCTIONS.LABEL_2');
		$scope.income_and_deductions_title   = $translate.instant('DASHBOARD.INCOME_AND_DEDUCTIONS.TITLE');
		$scope.income_and_deductions_help    = $translate.instant('DASHBOARD.INCOME_AND_DEDUCTIONS.HELP');

		$scope.totalIncomes = {
			barprogress:true,
			uncalculatePercent:true,
			title:$scope.income_and_deductions_label_1
		};

		$scope.totalDeductions = {
			barprogress: true,
			uncalculatePercent: false,
			title: $scope.income_and_deductions_label_2
		};

		/**
		 * @param type String
		 * @return Json Object
		 * @description Funcion que retorna objeto json con el tipo de usuario a consultar
		 **/
		$scope.http_data = function(type){
  		return angular.fromJson({'type': type});
  	};

		//Funcion que prepara los datos de ingresos y deducciones para la grafica
		$scope.data_to_graph_indeduc = function(income_and_deductions){
			var infoGraph = new Array();
			infoGraph['labelAxisX'] = [];
			infoGraph['values_graph1'] = [];
			infoGraph['values_graph2'] = [];
			angular.forEach(income_and_deductions, function(value){
				infoGraph['labelAxisX'].push($filter('date')(value['fpend'], 'MMM yyyy'));
				infoGraph['values_graph1'].push(parseFloat(value['devng']));
				infoGraph['values_graph2'].push(parseFloat(value['deduc']));
			});
			return infoGraph;
		}

		//Funcion para graficas de cesantias
		$scope.data_to_graph_severances = function(data_severances){
			var infoGraph = new Array();
			angular.forEach(data_severances,function(value){
				infoGraph.push([$filter('date')(value['fpend'],'M'),value['betrg']]);
			});
			return infoGraph;
		}

  	$scope.addToFavorite = function(newFavorite,position){
    	//Invoca al recurso y su metodo SAVE
    	favoriteEmployees.save({favorite_employee: newFavorite.pernr},function(response){
    		newFavorite.favorite_info  = response;
				newFavorite.image['image'] = newFavorite.image;
    		$scope.favorites['favorites'].push(newFavorite);
    		$scope.favorites['employees_remaining'].splice(position,1);
    		$scope.favorites['favorites'] = $filter('orderBy')($scope.favorites['favorites'],'short_name');
				$scope.new_style_favorites();
				$http({
					url: $scope.url_request('/favorite_employees'),
					method: 'GET',
					params: {'type':"favorites"}
				}).then(function (data) {
					$scope.favorites['favorites']=data.data;
				});
    	});
    }

    $scope.deleteToFavorite = function(delFavorite,position){
	    	favoriteEmployees.remove({id: delFavorite.pernr},function(response){
    		$scope.favorites['favorites'].splice(position,1);
    		$scope.favorites['employees_remaining'].push(delFavorite);
    		$scope.favorites['employees_remaining'] = $filter('orderBy')($scope.favorites['employees_remaining'],'short_name');
    		$scope.new_style_favorites();
    	});
    }

    $scope.new_style_favorites = function (){
			var newHeight ='135';
			if ($scope.favorites['favorites'].length > 1) {
				newHeight = $scope.favorites['favorites'].length * 50;
			}else if ($scope.favorites['favorites'].length === 1){
				newHeight ='130';
			}else if ($scope.favorites['favorites'].length === 0){
				newHeight ='50';
			}
    	if(newHeight > 250){
    		newHeight = 250;
    	}
    	$scope.style_favorites = {height: newHeight+"px"};
    	$scope.showSpinner 		 = false;
    }

    // function busca usuarios
		$scope.searching_nofavorites = function () {
			$scope.favorites.employees_remaining = [];
			$scope.showSpinner 				 = true;
			var name_search 					 = $scope.search_user[0].text.replace("-", " ");
			$scope.search_copy 				 = name_search;
		}

		$scope.addTag = function () {
			var logitud = $scope.search_user.length;
			var index   = logitud !== 0 ? logitud-1 : 0;

			if (typeof $scope.search_user[index] !== undefined) {
				$scope.search_user.pop();
			}
		}

		$scope.getAllUser = function () {
			$scope.search_copy 				 = "";
			$scope.favorites.employees_remaining = [];
			$scope.showSpinner 				 = true;
		}

  }]);
})();
