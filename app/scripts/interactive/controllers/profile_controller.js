(function(){
	'use strict';
	angular.module('Interactive')
	.controller('profileController',['$scope','data','$filter','$translate',function($scope,data,$filter,$translate){
		$scope.basic_profile = data.basic_data;
		$scope.formdata = {};
		$scope.data_master_info = angular.fromJson(data.data_master_info);
		$scope.active_item = 'datos_empresariales';
		$scope.active_menu = $scope.data_master_info;

		$scope.menu_data_master = {
			datos_empresariales:{label:$translate.instant('PROFILE.DATA.BUSINESS'),icon:'fa-building'},
			datos_bancarios:{label:$translate.instant('PROFILE.DATA.BANKING'),icon:'fa-credit-card'},
			datos_estudios:{label:$translate.instant('PROFILE.DATA.STUDIES'),icon:'fa-graduation-cap '},
			datos_contacto:{label:$translate.instant('PROFILE.DATA.CONTACT'),icon:'fa-info'},
			datos_personales:{label:$translate.instant('PROFILE.DATA.PERSONAL'),icon:'fa fa-user'},
			datos_familiares:{label:$translate.instant('PROFILE.DATA.FAMILY'),icon:'fa-users'},
			datos_beneficiarios:{label:$translate.instant('PROFILE.DATA.BENEFICIARIES'),icon:'fa-child'},
			datos_seguridadsocial:{label:$translate.instant('PROFILE.DATA.SOCIAL_SECURITY'),icon:'fa-stethoscope'},
			datos_impuestos:{label:$translate.instant('PROFILE.DATA.TAXES'),icon:'fa-university'},
			datos_retefuente:{label:$translate.instant('PROFILE.DATA.WITHHOLDING'),icon:'fa-money'}
		};

		$scope.active_menu = $scope.menu_data_master.datos_empresariales;
		angular.forEach(data.employee_info,function(obj,j){
			$scope.formdata[j] = new Array();
			if(obj != null){
				var row = -1;
				var iniRow = $filter('orderBy')(angular.fromJson(obj), 'pvisu')[0].pvisu;
				angular.forEach(angular.fromJson(obj), function(val,i){
					if(val.pvisu == iniRow){
						row += 1;
						$scope.formdata[j][row] = new Array();
					}
					if($scope.formdata[j][row] != undefined){
						$scope.formdata[j][row].push(val);
					}
				});
				$scope.menu_data_master[j]['disabled']="false";
			}else{
				$scope.menu_data_master[j]['disabled']="true";
			}
		});

		$scope.menu = function(slt_option){
			$scope.active_item = slt_option;
			$scope.active_menu = $scope.menu_data_master[slt_option];
		}

		$scope.classOptionMenu = function(key,disabledOption){
			var classToOption = "";
			if($scope.active_item == key){
				classToOption +=" active";
			}
			if(disabledOption == 'true'){
				classToOption +=" disabled ";
			}
			classToOption = classToOption.trim();
			return classToOption;
		};

		$scope.class_box_data = function(){
			if ($scope.responsive('>= 800') && $scope.responsive('<= 900')) {
				return 'form-group col-sm-6';
			}
			return 'form-group col-sm-4';
		}
	}]);
})();
