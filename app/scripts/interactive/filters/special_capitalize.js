(function(){
	'use strict';
  angular.module('Interactive')
  .filter('special_capitalize',[function(){
    return function (value,type){
      if (!value) return '';
      var words = value.split(" ");
      var newValue = "";
      var newWord;
      var exceptionsAcronyms = ['TI','BTL','SAP','S.A.S','S.A.','LTDA'];
      var namesNoPermited = ['del','de'];
      angular.forEach(words,function(word,iWord){
        if (exceptionsAcronyms.indexOf(word.toUpperCase()) != -1){
          newWord = word.toUpperCase()+" ";;
        }else if((word.length > 0 && word.length <= 3) && (type!="own" && namesNoPermited.indexOf(word.toLowerCase())> -1)){
          newWord = word.toLowerCase()+" ";
        }else{
          newWord = word.substring(0,1).toUpperCase() + word.substring(1).toLowerCase()+" ";
        }
        newValue = newValue.concat(newWord);
      });
      return newValue;
    }
  }]);


 
})();