(function () {
  'use strict'
    angular.module('Interactive')
    .directive('viewArticle', ['$rootScope','$sce',function($rootScope,$sce) {
      return {
        restrict: 'E',
        scope:{
          article:"=data",
          deleteArticle:'=fndelete'
        },
        link:function(scope,element, attr){
          if (scope.article !== undefined) {
            scope.article.$promise.then(function (response) {
              scope.article=response;
              scope.ifCreate =  angular.equals(scope.article.employee.pernr,$rootScope.$user.employee.pernr);
            });
          }
          scope.formatHtml = function( html ){
            return $sce.trustAsHtml(html);
          }
        },
        templateUrl: 'views/directives/view_article.html'
      };
    }]);
})();
