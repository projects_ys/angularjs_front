(function () {
  'use strict'
		angular.module('Interactive')
		    .directive('commentsBox', ['$rootScope','CommentArticle','$filter',function($rootScope,CommentArticle,$filter) {
		    	return {
				    restrict: 'E',
            scope:{
              article:"=data"
            },

				    link:function(scope,elements,attrs){
              scope.userImagen   = $rootScope.$user.employee.image.url;
              scope.article.$promise.then(function (response) {
                scope.newcomment.article_id= response.id;
                scope.loadComments();
              });

				    	scope.loadComments = function(){
                CommentArticle.get({articleId:scope.article.id},function(response){
                   scope.CommentArticle = response.comments;
			          });
				    	}

		          scope.createDate = function(created_date){
		          	var today = $filter('date')(new Date(), "dd.MM.yyyy");
		          	var date = $filter('date')(created_date, "dd.MM.yyyy");
		          	var hour = $filter('date')(created_date, "hh:mm:ss a");
		          	var dateReturned = "";

		          	if(today == date){
		          		dateReturned += "Hoy ";
		          	}
		          	dateReturned = dateReturned+hour+" - "+date;
		          	return dateReturned;
		          }
		          scope.newcomment = {
	              comment_text:"",
	              article_id:scope.article.id
	            };
		          scope.saveComment = function(){
		          	if(scope.newcomment.comment_text.trim() != ""){
			          	CommentArticle.save(scope.newcomment,function(response){
				            scope.CommentArticle = response.comments;
				            scope.newcomment.comment_text = "";
				            scope.loadComments();
				          });
			          }
		          }
            
				    },
				   	templateUrl: 'views/directives/comments_box.html'
				  };

		   	}]);
})();
