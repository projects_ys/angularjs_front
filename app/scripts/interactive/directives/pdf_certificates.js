/**
* pdfCertificates

* @description:
* Directiva que muestra los archivos pdfs de cualquier certificado del empleado
* @params:
* -type:'@'  => Pude contener los siguientes valores a ordenar ['date', 'string']
* -order:'=' => Tipo de ordenamiento, ['string_asc', 'string_desc', 'date']
* -data:'='  => Data con la que se va a manejar la directiva
*
*  @author Juan P. Contreras
*  19-07-2016 - creción y pruebas
*  28-12-2016 - Update   - PreLoad  -Jesus Alvarez
**/
(function () {
  'use strict'

  angular.module('Interactive')
  .directive('pdfCertificates', ['$state','$stateParams','CERTIFICATES','$translate',function($state,$stateParams,CERTIFICATES,$translate) {
    return {
      restrict: 'E',
      scope:{
        type: "@",
        order: "=",
        data: "="
      },
      link: function( scope, element, attributes ){
        //preload
        scope.show_loading = true;
        scope.progress=10;
        var message_error = function(certificate_type){
          scope.template_message_error = "views/certificate/error.html"
          if (certificate_type === CERTIFICATES.vacations_key){
            scope.msgError = $translate.instant('CERTIFICATES.NOT_VACATIONS_KEY');
          }else{
            scope.msgError = $translate.instant('CERTIFICATES.NOT_HAVE_PDF');
          }
        }

        scope.change_pdf = function(certificate_id, data){
          if (parseInt(certificate_id) !== scope.certificate_key) {
            scope.show_loading=true;
            scope.progress     = 0;
            scope.certificate_label = data.name_iden
            scope.certificate_key   = parseInt(certificate_id);
            scope.pdfUrl            = scope.pdfs[scope.certificate_key].file.url;
            $state.transitionTo(scope.data_file.state, {
              id: scope.certificate_key
            });
          }
        }

        scope.dropdown_active= function(key, data){
          var class_style = "";
          if (key === scope.certificate_key) {
            scope.certificate_label = data.name_iden
            class_style = "dropdown active";
          }
          return class_style;
        }

        attributes.$observe("type", function(value){
          scope.show_loading = true;
          scope.template_message_error = "";
          scope.exists_pdf             = true;
          scope.certificate_key        = isNaN(parseInt($stateParams.id)) ? 0 : parseInt($stateParams.id);
          scope.data_file              = JSON.parse(value);
          scope.certificate_type       = scope.data_file.type;
          /** @scope Array pdfs**/
          scope.pdfs   = [];
          /** @scope String pdfUrl [Necesaria para la directiva ng-pdf]**/
          scope.pdfUrl = "";

          if (scope.data.length !== 0) {
            scope.pdfs   = sort_dropdown(scope.data);
            scope.pdfUrl = scope.pdfs[scope.certificate_key].file.url;

          } else {
            scope.exists_pdf       = false;
            scope.show_loading = false;
            message_error(scope.certificate_type);
          }
        });

        // preload
        scope.onLoad = function() {
          scope.show_loading = false;
        };
        scope.onProgress = function (progressData) {
          scope.progress = (progressData.loaded / progressData.total)*100;
          //console.log(scope.progress);
        };
        // scope.onError = function(error) {
        //   console.log(error);
        // }

        //ordernar dropdown-menu de Certificados de Ingresos y retenciones
        var sort_dropdown = function (data) {
          if (data.length > 1) {
            switch(scope.order){
              case 'date':
                data.sort(function(a, b) {
                  var a = new Date(a.name_iden),
                      b = new Date(b.name_iden);
                  return a>b ? -1 : a<b ? 1 : 0;
                });
                break;
              case 'string_asc':
                data.sort(function(a,b) {
                  var a = a.name_iden.toLowerCase(),
                      b = b.name_iden.toLowerCase();

                  if (a < b) //sort string ascending
                      return -1
                  if (a > b)
                      return 1
                  return 0 //default return value (no sorting)
                });
                break;
              case 'string_desc':
                data.sort(function(a,b) {
                  var a = a.name_iden.toLowerCase(),
                      b = b.name_iden.toLowerCase();

                  if (a > b) //sort string descending
                      return -1
                  if (a < b)
                      return 1
                  return 0 //default return value (no sorting)
                });
                break;
            }
          }
          return data;
        };
      },
      templateUrl: 'views/directives/pdf_certificates.html'
    };

  }]);
})();
