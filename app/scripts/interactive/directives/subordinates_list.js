(function () {
  'use strict'

	angular.module('Interactive')
  .directive('subordinatesList', ['$http',function($http) {
  	return {
	    restrict: 'E',
	    scope:{
        srchttp:'@',
        function:'='
	    },
	    link:function(scope,elements,attrs){
        scope.profile_selected = 0;
        scope.filter           = {};
        scope.filter.type      = ''; 
        $http.get(scope.srchttp)
        .then(function(response) {
          scope.employees = response.data;
        }, function(response) {

        });

        scope.button_filter_active = function(type){
          return scope.filter.type === type ? 'active' : '';
        }

        scope.amount_filter = function(value){
          return "$"+parseInt(value).toLocaleString(undefined,{minimumFractionDigits: 0});
        }

        scope.active_profile = function(key, amount){
          if (amount !== 0) {
            return key === scope.profile_selected ? 'active' : '';
          }
          return 'row-inactive';
        }

        scope.show_profile = function(key, employee){
          if (!scope.is_vacant(employee)) {
            scope.function(employee);
            scope.profile_selected = key;
          }else{
            scope.function({});
            scope.profile_selected = 0;
          }
        }

        scope.is_vacant = function(employee){
          if (employee.id !== undefined) {
            return employee.type === 'vacante';
          }
          return true;
        }
      },
	    templateUrl: 'views/directives/subordinates_list.html'
	  };

 	}]);
})();
