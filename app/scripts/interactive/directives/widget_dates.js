/**
 *
 * Directivas Parametros
 *
 * @description
 * 	Widget Dates: Widget tipo tablas con scroll con comunicacion HTTP GET, requiere que la consulta
 * 	devuelva especificamente los siguientes valores:
 *
 * 	PARAMS
 *
 *	- src Url donde se ira a realizar la peticion HTTP GET para que retorne la consulta con las siguientes caracteristicas:
 *  - keylist Identificador del caja en donde se encuentra esta directiva (con fines de mostrar solo cuando tenga datos)
 *
 *   Parametros de arreglo de respuesta HTTP (Consulta con Alias)
 *
 *  - image.url
 *	- short_name
 *	- date_show (Fecha necesaria con ese ALIAS en la consulta, el widget mostrara formato dd MMM)
 *
 * 	@author Yeison A. Suarez
 * 	-- 15 Jul 2013: Creación y prueba de funcionamiento OK! Con las caracteristicas descritas
 *  @author Juan P. Contreras
 *	-- 08 Sep 2016: Agregar nuevo parametro para ocultar o mostrar la caja contenedora de esta directiva
 *
 */
 (function () {
 	'use strict'
 	angular.module('Interactive')
 	.directive('datesWidget', ['$http',function($http) {
 		return {
 			restrict: 'E',
 			transclude: true,
 			replace: true,
 			scope:{
 			},
 			link:function(scope,elements,attrs){
 				scope.currentDate = new Date();
 				scope.wait = true;
 				$http({method: 'GET', url:attrs['srchttp']}).then(function (result) {
 					scope.data = result.data;
 					if (scope.data.length >= 1) {
 						var element = angular.element(document.getElementById(attrs['keylist']));
 						element.removeClass("hide");
 						scope.wait = false;
 					}
 				}, function (error) {
 					console.info(error);
 					console.info("Error: No data returned");
 				});
 			},
 			templateUrl: 'views/directives/widget_dates.html'
 		};

 	}]);
 })();
