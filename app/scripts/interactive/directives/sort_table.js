/**
  * sortTable
  *
  * @description:
  * Crea una tabla con ordenamiento y busqueda por campos
  * @params:
  * -type:'@' => Array asociativo con los datos basicos:
  *     type: puede contener algunos de los siguientes acronimos referenciados al tipo de certificado:
  *         'VCTN', 'CIYR', 'CLAB', 'VPAG'. Estos acronimos los podemos encontrar en el modulo 'CERTIFICATES'
  *     state: Estado de la url donde debe apuntar cada certificado. Emplo: 'main.certificates.labor', para certificados laborales
  * -srchttp: => Url del recurso donde se debe consultar la data
  *
  *  @author Juan P. Contreras
  *  25-07-2016 - creción y pruebas
  **/
(function () {
  'use strict'

	angular.module('Interactive')
  .directive('sortTable', ['$http','$rootScope','$state','$stateParams','$q','PagerService',function($http,$rootScope,$state,$stateParams,$q,PagerService) {
  	return {
	    restrict: 'E',
	    scope:{
        srchttp:'@',
        pagesizerow:'='
	    },
      controller: ['$scope',function($scope){
        $scope.data_db       = {};
        $scope.seizure_label = 'Todos';
        $scope.data_any      = {value: 'Todos'}
        $scope.pager={};
        $scope.predicate     = '';
        $scope.reverse       = false;
        $scope.sort_status   = [];
        $scope.currentPage=1;
        $scope.classBtnPrev      = 'disabled';
        $scope.classBtnNext      = '';
        $scope.tableResponse  ="";
        $scope.items={};
        $scope.dropdown_labels=[];
        $scope.isResponse=false;
        var SORTING  = 1,
        SORTING_ASC  = 2,
        SORTING_DESC = 3;

        $scope.$watch("srchttp", function(value){
          $scope.data_db       = {};
          $scope.isResponse=false;
          $http.get($rootScope.url_request(value))
          .then(function(response) {
            $scope.isResponse=true;
            if(response.data.list !== null){
              $scope.data_db = response.data;
              $scope.dropdown_labels=[];
              angular.forEach($scope.data_db.labels, function(value, key){
                if(value.sortable){
                  $scope.dropdown_labels.push({ 'key':key, 'value':value });
                }
              });
              $scope.search_value = '';
              $scope.search       = '$';
              $scope.setPage(1,$scope.data_db.list.length);
              if($scope.data_db.error !==  undefined){
                if($scope.data_db.status ===  '500'){
                $state.transitionTo('error.500');
                }else if ($scope.data_db.status ===  '401') {
                  $state.transitionTo('error.permission');
                }
              }
            }

          }, function(response) {
            $scope.isResponse=true;

          });
        });

        $scope.total_rowspan = function(value){
          var rowspan = (value !== undefined) ? value.length : 0;
          return rowspan >= 1 ? rowspan + 1 : 0;
        }

        $scope.is_scalable = function(value){
          return value !== undefined;
        }

        $scope.get_data = function(data){
          return data['data'];
        }

        $scope.change_search = function(key, data){
          $scope.search = key;
          $scope.search_by = [];
          $scope.search_by[$scope.search] = $scope.search_value;
          $scope.seizure_label = data.value;
          $scope.search    = key;
        }

        $scope.searching = function(){
          $scope.search_by = [];
          $scope.search_by[$scope.search] = $scope.search_value;
          return $scope.search_by;
        }

        $scope.dropdown_active= function(key, data){
          var class_style = "";
          if (key === $scope.search) {
            $scope.seizure_label = data.value
            class_style = "dropdown active";
          }
          return class_style;
        }

        $scope.sort_class = function(index, data){
          if (data.sortable) {
            if ($scope.sort_status[index] === undefined || $scope.sort_status[index] === SORTING) {
              return "sorting"
            }else if($scope.sort_status[index] === SORTING_ASC){
              return "sorting_asc";
            }else{
              return "sorting_desc";
            }
          }else{
            return "sorting_disabled";
          }
        }

        $scope.sorting = function(index, key, data){
          if (data.sortable) {
            switch($scope.sort_status[index]){
              case undefined:
              case SORTING:
                $scope.sort_status[index] = SORTING_DESC;
                break;
              case SORTING_DESC:
                $scope.sort_status[index] = SORTING_ASC;
                break;
              case SORTING_ASC:
                //$scope.sort_status[index] = SORTING;
                $scope.sort_status[index] = SORTING_DESC;
                break;
            }

            angular.forEach($scope.sort_status, function(value, key2){
              if(key2 !== index){
                $scope.sort_status[key2] = SORTING;
              }
            });

            $scope.reverse = ($scope.predicate === key) ? !$scope.reverse : false;
            $scope.predicate = key;
          }
        }

        $scope.get_predicate = function(){
          return $scope.predicate;
        }

        $scope.get_reverse = function(){
          return $scope.reverse;
        }

        var set_zero = function(value){
          return value < 10 ? '0'+value : value;
        }

        $scope.field_bind = function(key, data){
          if ($scope.data_db.labels[key] !== undefined) {
            switch($scope.data_db.labels[key].type){
              case 'money':
                 return "$"+data.toLocaleString(undefined,{minimumFractionDigits: 0});
              case 'percentage' :
                 return parseInt(data).toLocaleString()+'% ';
              case 'int':
                return parseInt(data);
              case 'date':
              case 'string':
                return data;
              default:
                return data;
            }
          }
          return '';
        }

          $scope.value_class = function(key){
            var css_class = 'text-left';
            switch($scope.data_db.labels[key].type){
              case 'string':
                css_class = 'text-left';
              break;
              case 'money':
                css_class = 'text-right';
              break;
              case 'percentage':
                css_class = 'text-right';
              break;
              case 'date':
                css_class = 'text-left';
              break;
              case 'day':
                css_class = 'text-left';
              break;
              case 'int':
                css_class = 'text-right';
              break;
            }
            return css_class;
          }

        $scope.exists_data = function(data_db){
          if (data_db.list !== undefined && data_db.list !== null) {
            return data_db.list.length !== 0;
          }
          return false;
        }
        $scope.setPage =  function(page,logitud) {
            $scope.pager = PagerService.GetPager(logitud, page,$scope.pagesizerow);
            if ($scope.pager.totalPages <= 1) {
              $scope.classBtnNext      = 'disabled';
            }else{
              $scope.classBtnPrev      = 'disabled';
              $scope.classBtnNext      = '';
            }
            if($scope.pager.currentPage==1){
              $scope.classBtnPrev      = 'disabled';
            }else{
              $scope.classBtnPrev      = '';
            }
        }
        $scope.changePageIndex = function(index) {
          if (index!=$scope.pager.currentPage){
            if(  index  <= $scope.pager.totalPages  ){
            $scope.setPage(index,$scope.items.length);
            if(index == 1){
              $scope.classBtnPrev='disabled';
              $scope.classBtnNext='';
            }else
            if(index == $scope.pager.totalPages){
              $scope.classBtnPrev='';
              $scope.classBtnNext='disabled';
            }else {
              $scope.classBtnPrev='';
              $scope.classBtnNext='';
            }
           }
         }
        }
        $scope.classPageActive = function(index) {
          if($scope.pager.currentPage==index){
            return 'active';
          }
          return '';
        }
        $scope.prevBtn = function() {
          if($scope.classBtnPrev!='disabled'){
            $scope.setPage($scope.pager.currentPage-1,$scope.data_db.list.length);
          }
          if($scope.pager.currentPage == 1){
            $scope.classBtnPrev='disabled';
            $scope.classBtnNext='';
          }
        }
        $scope.nextBtn = function() {

          if($scope.classBtnNext!='disabled'){
            $scope.setPage($scope.pager.currentPage+1,$scope.data_db.list.length);
          }

          if($scope.pager.currentPage == $scope.pager.totalPages){
            $scope.classBtnPrev='';
            $scope.classBtnNext='disabled';
          }

        }
       $scope.$watch('items',function(newValue, oldValue, scope) {
         if(typeof newValue !="undefined"){
           $scope.setPage(1,newValue.length);
         }
      }, true);
        $rootScope.$watch('responsive()', function(newValue, oldValue, scope) {
              switch (newValue) {
              case 'large':
              $scope.tableResponse  ="";
                break;
              case 'medium':
              $scope.tableResponse  ="";
                break;
              case 'small':
              $scope.tableResponse  ="table-responsive";
                  break;
              case 'tiny':
              $scope.tableResponse  ="table-responsive";
                    break;
              default:

            }
      }, true);
      }],
	    templateUrl: 'views/directives/sort_table.html'
	  };

 	}]);
})();
