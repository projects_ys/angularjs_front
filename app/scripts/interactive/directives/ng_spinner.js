/**
  * ngSpinner 
  *
  * @description: 
  * 
  * @params:
  * type:@ => Tipo de spinner que se desea visualizar, estos spinner puenden ser de tipo:
  *    - 1 : cube-grid
  *
  *  @author Juan P. Contreras
  *  19-07-2016 - creción y pruebas
  **/
(function () {
  'use strict'
    angular.module('Interactive')
    .directive('ngSpinner', ['$rootScope','$http',function($rootScope,$http) {
      return {
        restrict: 'E',
        scope:{
          type:"@",
          loading:"=",
          size:"@"
        },
        link:function(scope,elements,attrs){
          scope.show_cube_grid = scope.type === "cube-grid";
          if(scope.size == undefined || scope.size == ''){
            scope.size = "30px";
          }
        },
        templateUrl: 'views/directives/ng_spinner.html'
      };
    }]);
})();