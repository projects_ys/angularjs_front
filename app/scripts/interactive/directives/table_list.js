/**
  * tableList
  *
  * @description:
  *
  * @params:
  * list:=     => Variable tipo Array donde se agregara la data
  * namelist:= => nombre o key del arreglo donde se agregara la data
  * callback:= => function
  * search:=   => Texto de busqueda que se añade como parametro en la ruta de peticion
  *
  * @attributes
  * srchttp  => Ruta donde se peticiona datos
  * httpdata => Texto formateado como Json que se utilizara como objeto de parametros ala ruta de peticion
  * initPage => Numero de la pagina desde donde se petionara datos
  *
  * @author: Juan Pablo Contreras
  * @date: 10/10/2016
  **/
(function () {
  'use strict'

	angular.module('Interactive')
  .directive('tableList', ['$http','$state','$stateParams','requestEvents','TYPES_REQUESTS',function($http,$state,$stateParams,requestEvents,TYPES_REQUESTS) {
  	return {
	    restrict: 'E',
	    scope:{
        srchttp:'@',
        srctype:'=',
        viewroute:'@',
        view:'@',
        nrecords:'='
	    },
      controller: ['$scope',function($scope){
        $scope.data                = {approved:[]};
        $scope.http_data           = {
          state: 'pending',
          approver: $scope.srctype,
        };
        $scope.filter              = {};
        $scope.filter.type         = "pending";
        $scope.search              = "";
        $scope.search_copy         = "";
        $scope.pendind_aprove_boss = TYPES_REQUESTS.PENDING_APPROVE_BOSS;

        $scope.currentPage       = 1;
        $scope.classBtnPrev      = "disabled";
        $scope.classBtnNext      = "";
        $scope.tableResponse     = "";
        $scope.position_records  = 0;  //
        $scope.page              = 1;  //Pagina Actual
        $scope.total_managed     = 0;  //Total Registros 'Solicitudes Gestionadas'
        $scope.pages_managed     = []; //Total paginas 'Solicitudes Gestionadas'
        $scope.status_search     = false;
        $scope.isloading         = true;
        $scope.isdata         = true;

        $scope.searching = function(){
          $scope.search_copy   = $scope.search;
          $scope.data.approved = [];
        }

        $scope.exists_data = function(){
          if ($scope.data.approved !== undefined && $scope.data.approved !== null) {
            return $scope.data.approved.length !== 0;
          }
          return false;
        }

        $scope.select_filter = function(){
          if ($scope.http_data.state !==$scope.filter.type ) {
            $scope.http_data.state = $scope.filter.type;
            $scope.search          = "";
            $scope.search_copy     = "";
            $scope.data.approved   = [];
            $scope.isloading         = true;
            $scope.isdata         = true;
          }

        }

        $scope.icon_states = function (state) {
          return requestEvents.request_icon_states(state);
        }
        $scope.btn_states = function (state) {
          return requestEvents.request_btn_states(state);
        }
        $scope.delete_request = function (value) {
          return requestEvents.delete_request(value);
        }

        $scope.route_directive = function(value){
          switch($scope.viewroute){
            case 'create': return 'main.request.create';
            case 'main.request.show': return 'main.request.show({request_id:'+value+'})';
            case 'main.approved.show':return 'main.approved.show({request_id:'+value+'})';
          }
        }

        $scope.$watch('responsive()', function(newValue, oldValue, scope) {
          switch (newValue) {
            case 'large':
            $scope.tableResponse  ="";
              break;
            case 'medium':
            $scope.tableResponse  ="";
              break;
            case 'small':
            $scope.tableResponse  ="table-responsive";
                break;
            case 'tiny':
            $scope.tableResponse  ="table-responsive";
                  break;
            default:
          }
        }, true);

      }],
	    templateUrl: 'views/directives/table_list.html'
	  };
 	}]);
})();
