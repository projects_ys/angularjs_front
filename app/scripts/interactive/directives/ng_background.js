(function () {
  'use strict'
  angular.module('Interactive')
  .directive('ngBackground', ['$state',function($state) {
    return {
      restrict: 'A',
      link:function(scope,elements,attrs){
        attrs.$observe("url", function(value){
          var element = angular.element(elements);
          if (value !== '') {
            element.backstretch(value);
          }else{
            element.backstretch('destroy');
          }
        });
      }
    };
  }]);
})();