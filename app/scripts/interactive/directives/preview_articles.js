(function () {
  'use strict'
    angular.module('Interactive')
    .directive('previewArticles', ['$rootScope','Article','SweetAlert',function($rootScope,Article,SweetAlert) {
      return {
        restrict: 'E',
        scope:{
          columns:'=datapreview',
          deleteArticle:'=fndelete',
          viewroute:'@viewroute',
          editroute:'@editroute',
          primarykey:'@pk'
        },
        link:function(scope,element,attrs){
          scope.employee_pernr = $rootScope.user.employee.pernr;
          //console.info(scope.viewroute);
        },
        templateUrl: 'views/directives/preview_articles.html'
      };
    }]);
})();
