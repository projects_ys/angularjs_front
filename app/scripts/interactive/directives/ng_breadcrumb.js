/**
  * ngBreadcrumb 
  *
  * @description: 
  * Permite que el usuario conozca la ruta de su ubicación en directorios y subdirectorios, y navegue a través de ella, 
  * utilizando la UI de la plantilla.
  * 
  * @params:
  * alist:@ => lista del mapeo de de la ruta en la que se encuentra. Se recomienda enviar un objeto json desde
  * la ruta, con este orden:
  * {
  *    title:'Certificados',     #Titulo Principal de la ruta
  *    list: [                   #Lista de las rutas que indica el recorrido seguido y la forma de regresar
  *      {
  *        url:'',               #Ruta o estado
  *        name: 'Certificados', #Nombre de la ruta
  *        state:''              #El estado: activo, para indicar que es la ruta actual
  *      },
  *      {
  *        url:'',
  *        name:'Vacaciones',
  *        state:'active'
  *      }
  *    ]
  *  }
  *}
  *
  *  @author Juan P. Contreras
  *  21-07-2016 - creción y pruebas
  **/
(function () {
  'use strict'
  angular.module('Interactive')
  .directive('ngBreadcrumb', ['$state',function($state) {
    return {
      restrict: 'E',
      link:function(scope,elements,attrs){
        scope.is_active = function(state){
          return state == 'active';
        }
        scope.is_route = function(route){
          return route != undefined;
        }
        attrs.$observe("alist", function(value){
          if(value !== ""){
            scope.data = JSON.parse(value);
          }else{
            console.info("Sin referencia de ruta");
          }
        });
      },
      templateUrl: 'views/directives/ng_breadcrumb.html'
    };
  }]);
})();