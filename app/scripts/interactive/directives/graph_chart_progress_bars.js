/**
 * @description
 * graphChartProgressBars: Directiva capaz de dibujar una grafica charJsCtrl, los parametros de espera son:
 * 		srchttp: Ruta relativa a donde debera apuntar la consulta de los datos, el array de llegada debe
 * 				tener las siguientes caracteristicas.
 * 				-data_graph: '@' Array que contendra los valores que se deberan dibujar en la grafica, eje X y eje Y
 * 							se espera que en el controlador de la vista donde es llamada la directiva, exista una
 * 							funcion en $scope que permita la manipulacion y preparacion de los elementos en forma
 * 							que se especifica en el atributo function.
 *
 * 				-totaloneinfo: '@' Datos para poder completar el panel derecho, en el debera viajar un objeto JSON con
 * 								las siguientes caracteristicas.
 * 								barprogress: true/false -> validara si se debe o no mostrar la barra de progreso
 * 								uncalculatePercent: true/false -> True para no validar porcentaje y mantener la barra
 * 								en 0%, false para que calcule el porcentaje
 * 								title: Texto con el titulo que llevara cada total
 * 								valfullpercent:(Opcional) En caso de uncalculte false, enviar el valor del 100% frente
 * 								al que se calculara el valor total, si este no es enviado se asumira que el total es
 * 								el 100%.
 *
 * 				-totaltwoinfo: '@' Las caracteristicas de totaloneinfo se deberan repetir, este ultimo dibujara el segundo
 * 				total el cual tiene soporte la grafica.
 *
 * 				-function: '=' Funcion en el controlador de la vista que intenta llamar la directiva, esta debera retornar
 * 							un array con los siguientes elementos
 * 							-labelAxisX: Labels que se imprimiran en el eje x
 * 							-values_graph1: Valores para dibujar en el eje Y la primera grafica, mantener orden frente a los del labels eje x
 * 							-values_graph2: Valores para dibujar en el eje Y la segunda grafica, mantener orden frente a los del labels eje x
 *
 * 		@ -> Enviarlo valor impreso {{}})
 * 		= -> Enviarlo valor scope directamente sin impresion
 *
 * 	@author Yeison A. Suarez
 * 	-- 15 Jul Creación y prueba de funcionamiento OK! Con las caracteristicas descritas
 *
 */
(function () {
  'use strict'
		angular.module('Interactive')
		    .directive('graphChartProgressBars', ['$rootScope','$http',function($rootScope,$http){
		    	return {
				    restrict: 'E',
				    transclude: true,
				    scope:{
				    	srchttp:'@',
				    	totaloneinfo: '@',
				    	totaltwoinfo: '@',
				    	function: '=',
      				keylist: '@'
				    },
				    controller:['$scope','$filter',function($scope, $filter){
				    	$scope.wait = true;
				    	//Peticion get a la consulta de saldos (Grafica)
							$http.get($scope.srchttp)
							    .then(function(response) {
                    if (response.data !== undefined) {
                      if (response.data.data_graph !== undefined) {
                        var element = angular.element(document.getElementById($scope.keylist));
             						element.removeClass("hide");
                        var incomeDeduct = $scope.function(response.data.data_graph);
                        $scope.lineData = {
    								        labels: incomeDeduct['labelAxisX'],
    										datasets: [
    								            {
    								            	label: "In",
    								                fillColor: "rgba(220,220,220,0.5)",
    								                strokeColor: "rgba(220,220,220,1)",
    								                pointColor: "rgba(220,220,220,1)",
    								                pointStrokeColor: "#fff",
    								                pointHighlightFill: "#fff",
    								                pointHighlightStroke: "rgba(220,220,220,1)",
    								                data: incomeDeduct['values_graph1']
    								            },
    								            {
    								                label: "Out",
    								                fillColor: "rgba(35,198,200,0.5)",
    								                strokeColor: "rgba(35,198,200,0.7)",
    								                pointColor: "rgba(35,198,200,1)",
    								                pointStrokeColor: "#fff",
    								                pointHighlightFill: "#fff",
    								                pointHighlightStroke: "rgba(35,198,200,1)",
    								                data: incomeDeduct['values_graph2']
    								            }
    								        ]
    								    };
    								    /**
    								     * Options for Line chart
    								     */
    								    $scope.lineOptions = {
    								        scaleShowGridLines : true,
    								        scaleGridLineColor : "rgba(0,0,0,.05)",
    								        scaleGridLineWidth : 1,
    										scaleLabel: function (valuePayload) {
    											return $filter('currency')(valuePayload.value, '$',0);
    										},
    										multiTooltipTemplate: function(chartData) {return  $filter('currency')(chartData.value, '$',0)},
    								        bezierCurve : true,
    								        bezierCurveTension : 0.4,
    								        pointDot : true,
    								        pointDotRadius : 4,
    								        pointDotStrokeWidth : 1,
    								        pointHitDetectionRadius : 20,
    								        datasetStroke : true,
    								        datasetStrokeWidth : 2,
    								        datasetFill : true
    								    };
    								    /**
    								    	** Totales de ingresos y deducciones traidos desde el servicio
    								    */
    								    $scope.totaloneinfo = angular.fromJson($scope.totaloneinfo);
    								    $scope.totaltwoinfo = angular.fromJson($scope.totaltwoinfo);

    								    //Obtiene el valor de los totales
    								    $scope.firsttotal = response.data.total_one;
    								    $scope.secondtotal = response.data.total_two;
    								    //Asignar el mismo valor para al ejecutar funcion generar el 100%
    								    if($scope.totaloneinfo.valfullpercent === undefined){
    								    	$scope.totaloneinfo.valfullpercent = response.data.total_one;
    								    }
    								    if($scope.totaltwoinfo.valfullpercent === undefined){
    								    	$scope.totaltwoinfo.valfullpercent = response.data.total_two;
    								    }
    								    //Funcion de calculo de porcentaje para barra progreso
    								    $scope.calculatingPercentage = function(valToCalculate,valPercentFull,empty){
    								    	if(empty === true){
    								    		return 0;
    								    	}else{
    								    		return (valToCalculate*100)/valPercentFull;
    								    	}
    								    };
    								    $scope.wait = false;
                      }else {

                      }
                    }
							    }, function(error) {
							    	console.info(error);
						        console.info("Error: No data returned");
							    });
				    }],
				    templateUrl: 'views/directives/graph_chart_progress_bars.html',
				  };
		   	}]);
})();
