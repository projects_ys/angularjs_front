(function () {
	'use strict'
	angular.module('Interactive')
	.directive('widgetTwoColumns', ['$http',function($http) {
		return {
			restrict: 'E',
			scope:{
				srchttp: '@',
				drawingtype : '@',
				labeltitle: '@',
				icon: '@',
				function: '=',
				titlewidget: '@titlewidget',
				help:'@help',
				type:'@',
				keylist: '@'
			},
			controller:['$scope','$filter',function($scope,$filter){
				$scope.total_months = 0;
				$scope.loading_spinner = true;
				var type 			 =  $scope.type;
				var monthNames = [];
				monthNames[1]  = "Ene";
				monthNames[2]  = "Feb";
				monthNames[3]  = "Mar";
				monthNames[4]  = "Abr";
				monthNames[5]  = "May";
				monthNames[6]  = "Jun";
				monthNames[7]  = "Jul";
				monthNames[8]  = "Ago";
				monthNames[9]  = "Sep";
				monthNames[10] = "Oct";
				monthNames[11] = "Nov";
				monthNames[12] = "Dic";
				
				$http.get($scope.srchttp)
				.then(function(result){
					if (result.data !== undefined ) {
						if (result.data.value !== undefined && parseInt(result.data.value) !== 0) {
							var element = angular.element(document.getElementById($scope.keylist));
	 						element.removeClass("hide");
	 						if($scope.drawingtype === 'graph'){
	 							var data_graph  = $scope.function(result.data.data_graph);
	 							var data_months = new Array();
	 							var months 			= new Array();
	 							var key_month   = 1;

	 							angular.forEach(data_graph, function(value, key){
	 								months.push([key_month, monthNames[value[0]]]);
	 								data_months.push([key_month, value[1]]);
									key_month += 1;
	 							});

								$scope.graphOptions = {
									series: {
										bars: {
											show: true,
											barWidth: 0.5,
											fill: true,
											lineWidth: 1,
											fillColor: "#fff"
										}
									},
									xaxis: {
										tickDecimals: 0,
										tickSize: [1, "month"],
										ticks: months,
										font: { size:11,color: "#fff" },
										showLastLabel: true,

									},
									yaxis: {
										tickLength:0,
										show:false
									},
									colors: ["#fff"],
									grid: {
										color: "#999999",
										hoverable: true,
										clickable: true,
										tickColor: "transparent",
										borderWidth: 0
									},
									legend: {
										show: false
									},
									tooltip: true,
									tooltipOpts: {
				            //content: "x: %x, y: %y"
				            content: function(months, xval, yval) {
				            	var content = yval;
				            	if( type === 'currency' ){
				            		content = $filter('currency')(content, '$', 0)
				            	}
				            	return content;
				            }
				          },
				        };

				        $scope.dataGraph = [
				        {
				        	label: "",
				        	data: data_months
				        }
				        ];

				        $scope.total_months = result.data.data_graph.length;
				      }

				      $scope.value = result.data.value;
				      $scope.loading_spinner = false;
	 					}else{
							$scope.loading_spinner = false;
						}
					}
		    },function(error){
		    	console.info(error);
		    });

				$scope.responsive_col = function(total_months, column){
					var col_5 = 'col-xs-5 text-right',
					col_6 = 'col-xs-6 text-right',
					col_7 = 'col-xs-7 text-right';
					if(column === 'graph'){
						return total_months <= 3 ? col_6 : col_7;
					}else if(column === 'total'){
						return total_months <= 3 ? col_6 : col_5;
					}
				}

			}],

			templateUrl: 'views/directives/widget_two_columns.html',
		};

	}]);
})();
