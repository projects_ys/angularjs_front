	(function () {
		'use strict'
		angular.module('Interactive')
		.directive('selectOptions', ['$filter',function($filter) {
			return {
				restrict: 'E', 
				require: 'ngModel',
				scope:{
					label:"@label",
					ccamp:"@value",
					ncamp:"@name",
					idcamp:"@id",
					ngModel:"=ngModel",
					attrvalue:"@attrvalue",
					disabled:"@dis",
					attroption:"@attroption"
				},
				link:function(scope,element,attrs,ngModel){  					    	
					function validOptions(){
						var newOptions = new Array();
						if(attrs.options){
							var options = angular.fromJson(attrs.options);
							if(attrs.depend != undefined && attrs.depend != ""){
								angular.forEach(options, function(option,iOpt){
									if(option[attrs.attrdepend] == ngModel.$$parentForm[attrs.depend].$modelValue){
										newOptions.push(option);
									}
								});
							}else{
								newOptions = options;
							}
						}
						return newOptions;
					};

					scope.optionsValid = validOptions();

					scope.newOptions = function(){
						if(attrs.depend != undefined){	
							scope.optionsValid = validOptions();
						}
					};

				},
				template: 	'<label for="{{idcamp}}" ng-bind=label></label>'+
				'<select class="form-control" id="{{idcamp}}" name="{{ncamp}}" ng-model="ngModel" ng-options="option[attrvalue] as option[attroption] for option in optionsValid" ng-click="newOptions()" ng-disabled="{{disabled}}">'+
				'</select>'
			};

		}]);
	})();