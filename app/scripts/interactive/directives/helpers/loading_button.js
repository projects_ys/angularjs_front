/**
 * loadingButton
 *
 * @description
 * Directiva que coloca un boton con la caracteristica de carga al darle click.
 *
 * @parameters
 *    -value: Texto que va a tener el boton
 *		-loading: Variable de carga, que por defecto debe ser false, esta variable sera la encargada de realizar la
 *              el inicio y fin de visualizacion del efecto de carga.
 *		-function: Funcion que se ejecutara despues de realizar el efecto de carga.
 *
 *
 * @author Juan P. Contreras
 * -- 09 Sep 2016 Creación y prueba de funcionamiento.
 **/
(function () {
	'use strict'
	angular.module('Interactive')
	.directive('loadingButton', ['$timeout',function($timeout) {
		return {
			restrict: 'E',
			scope:{
				value:"=",
				loading:"=",
				function:"="
			},
			link:function(scope,element,attrs){
         scope.on_click = function(){
		      scope.increment = 0;
					 var i = 0;
					 while (scope.loading) {
						i=i+1;
						$timeout(function() {
		          scope.loading += parseFloat('0.'+i);
					  }, scope.increment += 100);
					 }
          scope.function();
				 }
	     },
	    template: '<button ladda="loading" ng-click="on_click()" class="ladda-button btn btn-primary block full-width m-b" data-style="expand-right" ng-bind="value"></button>'
	  };
	}]);
})();
