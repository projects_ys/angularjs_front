(function () {
  'use strict'
		angular.module('Interactive')
		    .directive('inputHidden', ['$filter',function($filter) {
		    	return {
				    restrict: 'E', 
				    require: 'ngModel',
				    scope:{
				    	ccamp:"@value",
				    	ncamp:"@name",
				    	idcamp:"@id",
						ngModel:"=ngModel"
				    }, 
				    link:function(scope,element,attrs,ngModel){
				    	
				    },
				    template: '<input type="hidden" id="{{idcamp}}" name="{{ncamp}}" class="form-control" ng-model="ngModel">'      
				  };

		   	}]);
})();