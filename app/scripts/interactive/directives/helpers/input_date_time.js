(function () {
	'use strict'
	angular.module('Interactive')
	.directive('inputDateTime', ['$filter',function($filter) {
		return {
			restrict: 'E', 
			//require: 'ngModel',
			scope:{
				key:"=",
				dateinit:"=",
				ngmodel:"=",
				showerror:"=",
				startview:"@",
				minview:"@",
				format:"@",
				minutestep:"@",
				range:"=" //{days:77, date_first:moment(), date_last:moment()}
			}, 
			controller:['$scope',function($scope){
				$scope.show_error = false;

				$scope.$watch('ngmodel', function(newVal, oldVal){
					if(newVal !== '' && !moment(newVal, $scope.format, true).isValid()){
						$scope.ngmodel = '';
					}
				});

				$scope.$watch('showerror', function(newVal, oldVal){
					$scope.show_error = newVal;	
				});
						
				if($scope.range !== undefined){
					$scope.renderOn = 'end-date-changed';
				}else{
					$scope.renderOn = 'start-date-changed';
				}
				
				$scope.config = function (){
					return {
	          dropdownSelector: '#'+$scope.key, 
	          renderOn: $scope.renderOn,
	          startView: $scope.startview,
	          minView: $scope.minview,
	          modelType: $scope.format,
	          minuteStep: $scope.minutestep !== undefined ? $scope.minutestep : 5
	        };
      	}

        $scope.start_date_on_set_time = function () {
          $scope.$broadcast($scope.renderOn);
        }

        $scope.start_date_before_render = function ($view, $dates) {
          if ($scope.dateinit) {
            var activeDate = $scope.dateinit;

            $dates.filter(function (date) {
              return date.localDateValue() <= activeDate.valueOf()
            }).forEach(function (date) {
              date.selectable = false;
            });
          }

          if($scope.range !== undefined){
          	if ($scope.range.days) {
							var activeDate = moment($scope.range.date_first).add($scope.range.days, $scope.range.days > 1 ? 'days' : 'day');
          	}

				    $dates.filter(function (date) {
				      return date.localDateValue() >= activeDate.valueOf()
				    }).forEach(function (date) {
				      date.selectable = false;
				    });
					}
        }
			}],
			templateUrl: 'views/directives/input_date_time.html'
		};
	}]);
})();