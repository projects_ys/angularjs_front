(function () {
	'use strict'
	angular.module('Interactive')
	.directive('inputText', ['$filter',function($filter) {
		return {
			restrict: 'E',
			require: 'ngModel',
			scope:{
				label:"@label",
				ccamp:"@value",
				ncamp:"@name",
				idcamp:"@id",
				placeholder:"@placeholder",
				attrvalue:"@attrvalue",
				disabled:"@dis",
				attroption:"@attroption",
				ngModel:"=ngModel",
				requiredval:"=requiredval"
			},
			link:function(scope,element,attrs,ngModel){
	    	//if (!ngModel) return;
	    	if(attrs.options != "" && attrs.options != undefined){
	    		attrs.$observe('options',function(value){
	    			if(value !== ""){
	    				var options = angular.fromJson(attrs.options);
	    				angular.forEach(options, function(option,iOpt){
	    					if(option[attrs.attrvalue] === ngModel.$modelValue){
	    						ngModel.$setViewValue(option[attrs.attroption]);
	    					}
	    				});
	    			}
	    		});
	    	}

	    	switch(attrs.typedate){
	    		case 'CURR':
	    		ngModel.$formatters.push(function(value) {
	    			return $filter('currency')(parseFloat(value),'$',0);
	    		});
	    		break;
	    		case 'DATE':
	    		ngModel.$formatters.push(function(value) {
	    			return $filter('date')(value,'dd/MM/yyyy');
	    		});
	    		break;
	    		case 'NUM':
	    		ngModel.$formatters.push(function(value) {
	    			return $filter('number')(parseFloat(value),0);
	    		});
	    		break;
	    		default:

	    		break;
	    	}

	    	scope.$watch('ngModel + newNgModel',function(){
	    		if(scope.disabled == "false" && scope.newNgModel != undefined){
	    			ngModel.$setViewValue(scope.newNgModel);
	    		}else{
	    			scope.newNgModel = ngModel.$viewValue;
	    		}
	    	},function(newValue,oldValue){
	    		console.log(newValue,oldValue);
	    	});
	    },
	    template: '<label for="{{idcamp}}" ng-bind=label></label>'+
	    '<input type="text" placeholder="{{placeholder}}" id="{{idcamp}}" name="{{ncamp}}" class="form-control" ng-model="newNgModel" ng-disabled="{{disabled}}" required="{{requiredval}}" >'
	  };
	}]);
})();
