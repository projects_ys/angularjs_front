/**
  * infiniteScroll
  *
  * @description:
  *
  * @params:
  * list:=     => Variable tipo Array donde se agregara la data
  * namelist:= => nombre o key del arreglo donde se agregara la data
  * callback:= => function
  * search:=   => Texto de busqueda que se aÃ±ade como parametro en la ruta de peticion
  * isdata:=   => Variable boolean que cambia su valor en tru cuando exista datos en el recurso solicitado
  *
  * @attributes
  * srchttp  => Ruta donde se peticiona datos
  * httpdata => Texto formateado como Json que se utilizara como objeto de parametros a la ruta de peticion
  * initPage => Numero de la pagina desde donde se petionara datos
  *
  * @author: Yeison A.
  *
  * @author update: Juan Pablo Contreras
  * @date: 10/10/2016
  **/
(function () {
  'use strict'
  angular.module('Interactive')
  .directive('infiniteScroll', ['$http',function($http) {
    return {
      restrict: 'A',
      scope:{
        list:'=',
        namelist:'=',
        callback:'=',
        search:'=',
        isdata:'=',
        isloading:'='
      },
      link: function(scope,elements,attrs){
        var raw             = elements[0];
        var positionRecords = 0;
        var preScrollTop    = 0;
        var blockScroll     = false;
        var serviceStatus   = true;
        scope.page          = parseInt(attrs.initPage);
        /**
        * @scope boolean load_init
        * @description Varible de carga inicial (para no ejecutar la busqueda textual, en la priemera carga)
        **/
        scope.load_init     = true;
        scope.isloading     = attrs.isloading;

        /**
        * @var Array page_forward
        * @description Varible de carga donde se almacena los datos de la consulta a la siguiente pagina a consultar
        **/
        var page_forward    = [];

        /**
         * @param search String => Texto para busqueda en el recurso
         * @param httpdata Json => Parametros de busqueda en el recurso
         **/
        var init_data = function(search, httpdata){
          if(scope.page == 1){
            if(serviceStatus == true){
              loadnewdata(search, httpdata);
            }
          }
        }

        /**
        * @param Json httpData
        * @return Object http
        **/
        var http_data = function(httpData){
          return $http({
            url: attrs.srchttp,
            method: 'GET',
            params: httpData
          });
        }

        var data_forward = function(httpData){
          //scope.isloading =true;
          http_data(httpData)
          .then(function(response){
            scope.isloading =false;
            page_forward = [];
            if (scope.isdata !== undefined) {
              scope.isdata = false;
            }
            if(response.data.length === 0){
              blockScroll = true;
            }else{
              angular.forEach(response.data,function(val,i) {
                page_forward.push(val);
              });

              blockScroll   = false;
              serviceStatus = true;
              scope.page += 1;
            }
          }, function (error) {
            console.log(error);
            scope.isloading =false;
          });
        }

        /**
         * @param search String => Texto para busqueda en el recurso
         * @param httpdata String => string formateado como objeto json
         **/
        var loadnewdata = function(search, httpData){
          httpData      = angular.fromJson(httpData);
          httpData.page = scope.page;
          if(blockScroll == false){
            if(!search){
              blockScroll = true;
              httpData.search = undefined;
            }else{
              httpData.search = search;
            }

            try{
              if (scope.page > 1 && page_forward.length !== 0) {
                scope.list[attrs.namelist] = scope.list[attrs.namelist].concat(page_forward);
                if(attrs.callback !== "" && attrs.callback !== undefined){
                  scope.callback();
                }
                data_forward(httpData);
              }else{
                http_data(httpData)
                .then(function(response){
                  scope.isloading =false;
                  if (scope.isdata !== undefined) {
                    scope.isdata = false;
                  }
                  if(response.data.length === 0 && page_forward.length === 0){
                    blockScroll = true;
                  }else{
                    angular.forEach(response.data,function(val,i) {
                      scope.list[attrs.namelist].push(val);
                    });

                    if(attrs.callback !== "" && attrs.callback !== undefined){
                      scope.callback();
                    }
                    scope.page += 1;

                    httpData.page = scope.page;
                    data_forward(httpData);
                  }
                },function (error) {
                  console.log(error);
                  scope.isloading =false;
                });
              }
            }catch(error){
              console.info(error);
            }
          }
        }

        scope.$watch('search', function(newVal){
          if(!scope.load_init){
            scope.page  = 1;
            blockScroll = false;
            init_data(newVal, attrs.httpdata);
          }
          scope.load_init = false;
        });

        attrs.$observe('httpdata', function(newVal, oldValue){
          scope.page    = 1;
          blockScroll   = false;
          serviceStatus = true;
          init_data('', newVal);
        });

        elements.bind('scroll', function () {
          if (raw.scrollTop + raw.offsetHeight == raw.scrollHeight && serviceStatus == true) {
            if(scope.list[attrs.namelist].length <= 0){
              scope.page  = parseInt(attrs.initPage);
              blockScroll = false;
            }
            if(serviceStatus === true){
              loadnewdata(scope.searching, attrs.httpdata);
            }
          }
        });
      }
    };
  }]);
})();
