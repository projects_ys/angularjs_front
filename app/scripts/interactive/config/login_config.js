(function(){
  'use strict';
  angular.module('Interactive')
  .config(['HRAPI_CONF','RUNENV_CONF','$authProvider','$translateProvider',function(HRAPI_CONF,RUNENV_CONF,$authProvider,$translateProvider){
    //Configuracion de internacionalizacion (Uso de archivo .json, con el diccionario de datos por sociedad)
    $translateProvider.useStaticFilesLoader({
      prefix : HRAPI_CONF.translate_file_location(),
      suffix: '.json'
    });
    $translateProvider.preferredLanguage(HRAPI_CONF.current_domain);
    $translateProvider.useSanitizeValueStrategy('escape');

    $authProvider.configure({
      apiUrl:                  HRAPI_CONF.apiBaseUrl(''),
      tokenValidationPath:     '/auth/validate_token',
      signOutUrl:              '/auth/sign_out',
      emailRegistrationPath:   '/auth',
      accountUpdatePath:       '/auth',
      accountDeletePath:       '/auth',
      confirmationSuccessUrl:  window.location.href,
      passwordResetPath:       '/auth/password',
      passwordUpdatePath:      '/auth/password',
      passwordResetSuccessUrl: window.location.href,
      emailSignInPath:         '/auth/sign_in',
      storage:                 'cookies',
      forceValidateToken:      false,
      validateOnPageLoad:      true,
      proxyIf:                 function() { return false; },
      proxyUrl:                '/proxy',
      omniauthWindowType:      'sameWindow',
      tokenFormat: {
        "access-token": "{{ token }}",
        "token-type":   "Bearer",
        "client":       "{{ clientId }}",
        "expiry":       "{{ expiry }}",
        "uid":          "{{ uid }}"
      },
      parseExpiry: function(headers) {
        // convert from UTC ruby (seconds) to UTC js (milliseconds)
        return (parseInt(headers['expiry']) * 1000) || null;
      },
      handleLoginResponse: function(response) {
        return response.data;
      },
      handleAccountUpdateResponse: function(response) {
        return response.data;
      },
      handleTokenValidationResponse: function(response) {
        return response.data;
      }
    });
  }]);
})();