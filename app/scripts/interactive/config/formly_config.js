(function(){
  'use strict';
  angular.module('Interactive')
  .config(['formlyConfigProvider',function(formlyConfigProvider){

    formlyConfigProvider.setWrapper([{
      templateUrl: 'views/formly/base_wrapper_input.html'
    }]);

    formlyConfigProvider.setWrapper({
      name: 'validationForModel',
      template: [
        '<div>',
          '<label for="{{::id}}">',
            '{{to.label}} {{to.required ? "*" : ""}}',
          '</label>',
          '<formly-transclude></formly-transclude>',
          '<div class="validation">',
            '<span class="text-danger" ng-if="model[options.key] === \'\' && model[\'show_error\']" >{{options.templateOptions.label}} es requerido.</span>',
          '</div>',
        '</div>'
      ].join(' ')
    });

    formlyConfigProvider.setWrapper({
      name: 'validationForInputDOM',
      template: [
        '<div>',
          '<label for="{{::id}}">',
            '{{to.label}} {{to.required ? "*" : ""}}',
          '</label>',
          '<formly-transclude></formly-transclude>',
          '<div class="validation">',
            '<span class="text-danger" ng-if="options.formControl.$error.required && model[\'show_error\']" >{{options.templateOptions.label}} es requerido.</span>',
          '</div>',
        '</div>'
      ].join(' ')
    });

    formlyConfigProvider.setWrapper({
      name: 'normalBootstrapLabel',
      template: [
        '<label for="{{::id}}">',
          '{{to.label}} {{to.required ? "*" : ""}}',
        '</label>',
        '<formly-transclude></formly-transclude>'
      ].join(' ')
    });

    formlyConfigProvider.setWrapper({
      name: 'horizontalBootstrapLabel',
      template: [
        '<label for="{{::id}}" class="col-sm-2 control-label">',
          '{{to.label}} {{to.required ? "*" : ""}}',
        '</label>',
        '<div class="col-sm-10">',
          '<formly-transclude></formly-transclude>',
        '</div>'
      ].join(' ')
    });
    formlyConfigProvider.setWrapper({
      name: 'horizontalBootstrapCheckbox',
      template: [
        '<div class="col-sm-offset-2 col-sm-10">',
          '<formly-transclude></formly-transclude>',
        '</div>'
      ].join(' ')
    });
    formlyConfigProvider.setWrapper({
      name: 'horizontalBootstrapSelect',
      template: [
        '<div class="col-sm-10">',
          '<formly-transclude></formly-transclude>',
        '</div>'
      ].join(' ')
    });

    formlyConfigProvider.setType({
      name: 'text_date_return_input',
      templateUrl: 'views/formly/text_date_return_input.html',
      controller: ['$scope',function($scope){
        $scope.$watch('model.date_begin', function(value){
          $scope.model.date_return = '';
          if(value !== '' && value !== undefined){
            var date_ini    = moment(new Date(value.split("-")));
            var days        = moment.duration($scope.model.available_days, 'd');
            var return_date = date_ini.add(days).format('LLLL');

            $scope.model.date_return = return_date;
          }
        });
      }]
    });
    formlyConfigProvider.setType({
      name: 'input_number',
      templateUrl: 'views/formly/input_number.html'
    });
    formlyConfigProvider.setType({
      name: 'input_number_dates',
      templateUrl: 'views/formly/input_number.html',
      controller: ['$scope',function($scope){
        var get_diff_days_and_hours = function(date1, date2){
          $scope.date_ini = moment(new Date(date1.split("-")));
          $scope.date_fin = moment(new Date(date2.split("-")));
          var diff_days  = parseInt($scope.date_fin.diff($scope.date_ini, 'days'));
          var diff_hours = parseInt($scope.date_fin.diff($scope.date_ini, 'hours'));
          return {
            days: diff_days >= 0 ? diff_days+1 : diff_days,
            hours:diff_hours
          };
        }

        $scope.$watch('model.date_begin', function(value){
          if((value !== '' && value !== undefined) && $scope.model.date_end !== ''){
            var diff = get_diff_days_and_hours(value, $scope.model.date_end);
            $scope.model.days_between_dates  = diff.days;
            $scope.model.hours_between_dates = diff.hours;
          }
        });

        $scope.$watch('model.date_end', function(value){
          if((value !== '' && value !== undefined) && $scope.model.date_begin !== ''){
            var diff = get_diff_days_and_hours($scope.model.date_begin, value);
            $scope.model.days_between_dates  = diff.days;
            $scope.model.hours_between_dates = diff.hours;
          }
        });
      }]
    });
    formlyConfigProvider.setType({
      name: 'single_file',
      templateUrl: 'views/formly/file_input.html'
    });
    formlyConfigProvider.setType({
      name: 'widget1',
      templateUrl: 'views/formly/widget_1.html'
    });
    formlyConfigProvider.setType({
      name: 'date_picker',
      wrapper: ['validationForModel'],
      templateUrl: 'views/formly/date_picker.html'
    });
    formlyConfigProvider.setType({
      name: 'horizontalInput',
      extends: 'input',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });
    formlyConfigProvider.setType({
      name: 'horizontalCheckbox',
      extends: 'checkbox',
      wrapper: ['horizontalBootstrapCheckbox', 'bootstrapHasError']
    });
    formlyConfigProvider.setType({
      name: 'horizontalSelect',
      extends: 'select',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });
    // Group Select
    formlyConfigProvider.setType({
      name: 'grouped_select',
      wrapper: ['normalBootstrapLabel', 'bootstrapHasError'],
      templateUrl:'views/formly/group_select.html'
    });

    //Config FileUpload of Resquest
    formlyConfigProvider.setType({
      name: 'file_request_input',
      extends: 'input',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
      templateUrl: 'views/formly/file_input.html',
      link: function(scope, el, attrs) {
        el.on("change", function(changeEvent) {
          var file = changeEvent.target.files[0];
          var resultBin="..";
          if (file) {
            var fd = new FormData();
            fd.append(file['name'], file);
            scope.$emit('fileToUpload', file);
            var fileProp = {};
            for (var properties in file) {
              if (!angular.isFunction(file[properties])) {
                fileProp[properties] = file[properties];
              }
            }
            scope.fc.$setViewValue(fileProp);
          } else {
            scope.fc.$setViewValue(undefined);
          }
        });
        el.on("focusout", function(focusoutEvent) {
          if (scope.id) {
            scope.$apply(function(scope) {
              scope.fc.$setUntouched();
            });
          } else {
            scope.fc.$validate();
          }
        });
      },
      defaultOptions: {
        validators: {
          fileInvalidExten: function($viewValue, $modelValue,$scope) {
            var value = $modelValue || $viewValue;
            var excFile="";
            if (value) {
              if( typeof value.name != "undefined"){
                excFile = value.name.substr(value.name.lastIndexOf(".")).toLowerCase();
                if( ($scope.permission_file.indexOf(excFile)=='-1') ){
                  return false;
                }else {
                  return true;
                }
              }else {
                return true;
              }
            }else{
              return true
            }
          },
          fileInvalidSize: function($viewValue, $modelValue,$scope) {
            var value = $modelValue || $viewValue;
            if (value) {
              if( typeof value.name != "undefined"){
                if(  (value.size > $scope.filesizeMax) ){
                  return false;
                }else {
                  return true;
                }
              }else {
                return true;
              }
            }else{
              return true
            }
          }
        },
        templateOptions: {
          type: 'file'
        }
      }
    });
    ///  formly content
    formlyConfigProvider.setType({
      name: 'summernote_input',
      wrapper: ['normalBootstrapLabel', 'bootstrapHasError'],
      templateUrl:'views/formly/summernote_input.html'
    });
    formlyConfigProvider.setType({
      name: 'onoffswitch',
      templateUrl:'views/formly/onoffswitch.html'
    });
    formlyConfigProvider.setType({
      name: 'textarea_input',
      wrapper: ['normalBootstrapLabel', 'bootstrapHasError'],
      templateUrl:'views/formly/textarea.html'
    });
    formlyConfigProvider.setType({
      name:"text_input",
      extends: 'input',
      wrapper: ['normalBootstrapLabel', 'bootstrapHasError'],
      templateUrl:'views/formly/text_input.html'
    });
    formlyConfigProvider.setType({
      name: 'email_login_input',
      wrapper: [ 'bootstrapHasError'],
      templateUrl:'views/formly/email_login_input.html'
    });
    formlyConfigProvider.setType({
      name: 'password_login_input',
      wrapper: [ 'bootstrapHasError'],
      templateUrl:'views/formly/password_login_input.html'
    });
  }]);
})();
