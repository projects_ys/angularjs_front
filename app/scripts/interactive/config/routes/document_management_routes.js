(function(){
  'use strict';
  angular.module('Interactive')
  .config(['$stateProvider',function($stateProvider) {
    $stateProvider
    .state('main.document_management', {
      abstract: true,
      url: "/documents",
      template: '<ui-view />'
    })
    .state('main.document_management.index', {
      url: '/all',
      data: { pageTitle: 'Gestion Documental',
        breadcrumb: {
          title:'NAVIGATION.DOCUMENT_MANAGEMENT',
          list: [
          {
            name: 'NAVIGATION.DOCUMENT_MANAGEMENT',
            state:'active'
          }
          ]
        }
      },
      templateUrl: 'views/document_management/index.html',
      resolve:{
        validate: ['$rootScope','$timeout','$state',function($rootScope,$timeout,$state){
          return function(){
            if (angular.equals($rootScope.$company.show_document_management, false)) {
              $timeout(function(){ $state.go('error.permission'); }, 10);
            }
          }
        }],
        type_documents: ['$rootScope','DocumentManagement',function($rootScope,DocumentManagement){
          return function () {
            return DocumentManagement.type_documents($rootScope.$company.id);
          }
        }],
        documents: ['$rootScope','DocumentManagement',function($rootScope,DocumentManagement){
          return function () {
            return DocumentManagement.documents($rootScope.$company.id);
          }
        }]
      },
      onEnter: ['$rootScope','validate','type_documents','documents',function($rootScope,validate,type_documents,documents){
        validate();
        $rootScope.type_documents = type_documents();
        $rootScope.documents      = documents();
      }],
      onExit: ['$rootScope',function($rootScope){
        $rootScope.type_documents = [];
        $rootScope.documents      = [];
      }],
      controller: ['$scope','$timeout',function($scope,$timeout){
        $scope.filter_type = 1;
        $scope.is_response = false;

        
        $timeout(function(){
          $scope.is_response = true;            
        }, 1000);
        

        $scope.select_filter = function(type_id){
          $scope.filter_type = type_id;
        }

        $scope.filter_active = function(type){
          return $scope.filter_type === type ? 'fa fa-folder-open' : 'fa fa-folder';
        }
      }]
    });
  }]);
})();
