(function(){
  'use strict';
  angular.module('Interactive')
  .config(['$stateProvider',function($stateProvider) {
    $stateProvider
    .state('main.request',{
      abstract:true,
      url:'/request',
      resolve: {
        loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
          {
            serie: true,
            files: ['scripts/inspinia/plugins/iCheck/custom.css','scripts/inspinia/plugins/iCheck/icheck.min.js']
          }
          ]);
        }
        ]
      },
      controller: 'RequestsController',
      template: '<ui-view />'
    })
    .state('main.request.index',{
      url:'/all',
      data: { pageTitle: 'NAVIGATION.REQUEST',
        breadcrumb: {
          title:'NAVIGATION.REQUEST',
          list: [
          {
            name: 'NAVIGATION.REQUEST',
            state:'active'
          }
          ],
          helpId: 7
        },
      },
      onEnter: ['$rootScope','$state','$timeout',function($rootScope,$state,$timeout){
        // valida si tiene permisos de compañia
        if($rootScope.$company.make_request){
          $rootScope.view = 'index';
        }else{
          $timeout(function(){ $state.go('error.permission'); }, 500);
        }
      }],
      onExit: ['$rootScope',function($rootScope){
        $rootScope.view = '';
      }],
      templateUrl:'views/requests/index.html'
    })
    .state('main.request.show',{
      url:"/view/:request_id",
      data: { pageTitle: 'NAVIGATION.REQUEST_DETAIL',
        breadcrumb: {
          title:'NAVIGATION.REQUEST_DETAIL',
          list: [
          {
            url:'main.request.index',
            name: 'NAVIGATION.REQUEST',
            state:''
          },
          {
            name: 'NAVIGATION.REQUEST_DETAIL',
            state:'active'
          }
          ],
          helpId: 7
        }
      },
      resolve :{
        request_params : ['$stateParams',function ($stateParams) {
          return function () {
            return $stateParams.request_id;
          }
        }]
      },
      onEnter: ['$rootScope','request_params','Request','$timeout',function($rootScope,request_params,Request,$timeout){
        // valida si tiene permisos de compañia
        if($rootScope.$company.make_request){
          $rootScope.view    = 'show';
            $rootScope.request = Request.get({id:request_params()});
        }else{
          $timeout(function(){ $state.go('error.permission'); }, 500);
        }
      }],
      onExit: ['$rootScope',function($rootScope){
        $rootScope.view    = '';
        $rootScope.request = [];
      }],
      templateUrl:'views/requests/show.html'
    })
    .state('main.request.create',{
      url:'/new',
      data: { pageTitle: 'NAVIGATION.REQUEST_NEW',
        breadcrumb: {
          title:'NAVIGATION.REQUEST_NEW',
          list: [
          {
            url:'main.request.index',
            name: 'NAVIGATION.REQUEST',
            state:''
          },
          {
            name: 'NAVIGATION.REQUEST_NEW',
            state:'active'
          }
          ],
          helpId: 7
        }
      },
      resolve:{
        data: ['RequestType',function(RequestType){
          return RequestType;
        }]
      },
      onEnter: ['$rootScope','data','$timeout','$translate',function($rootScope,data,$timeout,$translate){
        // valida si tiene permisos de compañia
        if($rootScope.$company.make_request){
          $rootScope.view          = 'create';
          $rootScope.types         = data.types_request;
          $rootScope.type_request  = [];
          $rootScope.model_request = {};
          $rootScope.errors        = [];
          angular.forEach($rootScope.types, function(value, key){
            $rootScope.type_request.push({name: value.description, value: value.form, group : $translate.instant('REQUEST.GROUP.'+value.id_activity)});
          });
          $rootScope.fields = [
            {
              key: 'type_id',
              type: 'grouped_select',
              className: '',
              templateOptions: {
                label: 'Tipo',
                options: $rootScope.type_request,
                required: true
              }
            }
          ];
        }else{
          $timeout(function(){ $state.go('error.permission'); }, 500);
        }
      }],
      onExit: ['$rootScope',function($rootScope){
        $rootScope.view          = '';
        $rootScope.types         = [];
        $rootScope.type_request  = [];
        $rootScope.fields        = [];
        $rootScope.model_request = {};
        $rootScope.errors        = [];
      }],
      templateUrl:'views/requests/create.html'
    });
  }]);
})();
