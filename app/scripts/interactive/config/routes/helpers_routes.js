(function(){
  'use strict';
  angular.module('Interactive')
  .config(['$stateProvider',function($stateProvider) {
    $stateProvider
    .state('main.help', {
      abstract: true,
      url: "/helps",
      resolve:{
        helpers_index:[function(){
          return [
            {id: 1, name:'Inicio'},
            {id: 2, name:'Organigrama'},
            {id: 3, name:'Perfil'},
            {id: 4, name:'Autoservicios'},
            {id: 5, name:'Consultas'},
            {id: 6, name:'Servicios gerenciales'},
            {id: 7, name:'Gestor de contenidos'},
            {id: 8, name:'Solicitudes'},
            {id: 9, name:'Aprobaciones'}
          ];
        }],
        helper_show:[function(){
          return [
            { 
              id: 1, name:'Inicio', description: 'La sección de <strong>inicio</strong> de inicio tiene como objetivo presentar información importante para los usuarios, de una forma amigable y gráfica. Para acceder, encontraras el siguiente icono <img alt="image" width="70" height="28" class="img-sm" src="https://s3.amazonaws.com/hrsolutions/document_help/home/icono.png"> , en el menu del lado izquierdo del navegador.',
              steps: [
                {name: '1. Vista completa', images: ['home/home.png'], description: 'En esta sección encontraras datos importantes como: <ul><li>Días de vacaciones</li><li>Cesantías e Intereses de cesantías</li><li>Ingresos y deducciones</li><li>Búsqueda de colaboradores con opción de añadir a favoritos</li><li>Colaboradores nuevos en la compañía</li><li>Cumpleaños de colaboradores</li><li>Colaboradores que estén de aniversario en la compañía</li></ul>'},
                {name: '2. Datos generales', images: ['home/graficos_1.png'], description: 'Tienes un resumen gráfico con información relacionada de los días de vacaciones que tienes para disfrutar e información de los últimos 3 meses de tus cesantías y el saldo de las mismas.'},
                {name: '3. Gráfica de ingresos y deducciones', images: ['home/graficos_2.png'], description: 'En esta gráfica podrás visualizar el total de tus ingresos y deducciones de los últimos 3 meses.'},
                {name: '4. Búsqueda de colaboradores', images: ['home/busqueda.png'], description: 'Tienes disponible un listado de todos los colaboradores de la compañía con información de correo electrónico o de su número telefónico, adicional a esto cuentas con una opción de búsqueda, donde puedes localizar el contacto que necesites y crear tu propia lista de hasta 10 favoritos.'},
              ]
            },
            { 
              id: 2, name:'Organigrama', description: 'La sección de <strong>organigrama</strong> extrae información relacionada a la estructura jerárquica y organizativa de la compañía, presentando en estructura de árbol las relaciones de un colaborador entre su jefe,  compañeros y sus subordinados. Para acceder, encontraras el siguiente icono <img alt="image" width="70" height="28" class="img-sm" src="https://s3.amazonaws.com/hrsolutions/document_help/organigram/icono.png"> , en el menu del lado izquierdo del navegador.',
              steps: [
                {name: '1. Vista principal', images: ['organigram/tree_complete.png'], description: 'Muestra por defecto la posición organizativa del usuario dentro de la organización. En la parte Superior al “Jefe Directo”, Nivel intermedio muestra los detalle de la posición del" usuario", y en nivel inferior muestra los "Subordinados".'},
                {name: '2. Filtros', images: ['organigram/search.png'], description: 'Se muestra campo de búsqueda de empleados, ingresando Nombre o Apellido se despliega lista de posibles resultados de empleados, visualizando imagen y datos del empleado.'},
                {name: '3. Navegación', images: ['organigram/lateral.png', 'organigram/inferior.png', 'organigram/superior.png'], description: '<ul><li><strong>Laterales:</strong>  Se mantiene en el mismo nivel, y presenta los compañeros, que son subordinados del nivel anterior.</li><li><strong>Inferiores:</strong> Se baja al nivel inferior , se muestra en nivel final a los subordinados del colaborador seleccionado.</li> <li><strong>Superiores:</strong> Se sube al nivel próximo, visualizando en primer nivel al jefe del colaborador seleccionado</li></ul>'}
              ]
            },
            {
              id: 3, name:'Perfil', description: 'La seccion de <strong>perfil</strong> encontraras toda tu información personal y laboral, aquí podrás encontrar información básica como: la fecha de inicio de tu contrato, los datos de tus familiares, tus datos de contacto, información financiera como: retención en la fuente y muchos más. Para acceder, encontraras el siguiente icono <img alt="image" width="70" height="28" class="img-sm" src="https://s3.amazonaws.com/hrsolutions/document_help/profile/icono.png"> , en el menu del lado izquierdo del navegador.',
              steps: [
                {name: '1. Vista completa', images: ['profile/profile.png'], description: 'En esta sección encontrarás toda la información que la empresa tiene registrada sobre ti. Los datos que se podrás consultar son: <ul><li>Empresariales</li><li>Bancarios</li><li>Estudios</li><li>Contacto</li><li>Personales</li><li>Familiares</li><li>Beneficiarios</li><li>Seguridad social</li><li>Impuestos</li><li>Retefuente</li></ul>'}, 
              ]
            },
            {
              id: 4, name:'Autoservicios', description: 'La sección de <strong>autoservicios</strong> podrás encontrar los archivos relacionados con tus volantes de pago, certificaciones laborales, certificado de ingresos y retenciones y cartas de vacaciones. Para acceder, encontraras el siguiente icono <img alt="image" width="70" height="28" class="img-sm" src="https://s3.amazonaws.com/hrsolutions/document_help/autoservices/icono.png"> , en el menu del lado izquierdo del navegador. Cuando des clic sobre el icono, se desplegará un listado en donde puedes seleccionar el archivo que desees ver.',
              steps: [
                {name: '1. Visualización y descarga', images: ['autoservices/botones.png', 'autoservices/botones_2.png'], description: 'Una vez seleccionado el archivo que desees ver, se habilitará un panel de opciones en la  parte superior las cuales te permitirán ampliar o disminuir el tamaño al visualizar un archivo o descargarlo.'},
                {name: '2. Cambiar el PDF', images: ['autoservices/botones_3.png'], description: 'También tienes la opción de seleccionar el periodo que desees visualizar, esto para los comprobantes de pago y los certificados de ingresos y retenciones.'}
              ]
            },
            {
              id: 5, name:'Consultas', description: 'La sección de <strong>consultas</strong> podrás observar información detallada de ingresos, prestamos, saldos vacaciones entre otros. Para acceder, encontraras el siguiente icono <img alt="image" width="70" height="28" class="img-sm" src="https://s3.amazonaws.com/hrsolutions/document_help/consultations/icono.png"> , en el menu del lado izquierdo del navegador. Cuando des clic  sobre el icono, se desplegará un listado en donde puedes seleccionar la consulta que deseas realizar.',
              steps: [
                {name: '1. Seleccionar consulta', images: ['consultations/consultations.png'], description: 'Una vez seleccionada la consulta que deseas realizar, podrás ver una tabla con la información  ordenada de la fecha más reciente hasta la fecha más antigua.'},
                {name: '2. Filtros', images: ['consultations/search_and_filter.png'], description: 'En la parte superior derecha encontrarás los filtros para facilitar las búsquedas. Tienes la opción de seleccionar el campo que deseas filtrar, para luego ingresa el detalle de la búsqueda.'},
                {name: '3. Ordenamiento', images: ['consultations/orderby.png'], description: 'En algunos titulos de la tabla de consulta, se muestra unas flechas paralelas cruzadas, estas nos indican que se puede realizar ordenamientos por campo.'},
                {name: '4. Paginación', images: ['consultations/paginate.png'], description: 'En la parte inferior derecha de la tabla se encuentra el total de regitros que existe y la pagina en la que se encuentra.'}
              ]
            },
            {
              id: 6, name:'Servicios gerenciales', description: '<p>Esta seccion se encarga de mostrar información de la historia laboral del empleado, posiciones y salarios en su historia en la compañía, y mostrar información consolidada de los conceptos de nóminas de los últimos 2 meses (Tomando en cuenta como mes los 2 períodos que la conforman).</p> <p>La información que se puede consultar es: Absentismos, Análisis de variaciones, Histórico de cargos, Histórico de sueldos y Rotación de personal</p> <p>Para acceder, encontraras el siguiente icono <img alt="image" width="70" height="28" class="img-sm" src="https://s3.amazonaws.com/hrsolutions/document_help/management_services/icono.png"> , en el menu del lado izquierdo del navegador.',
              steps: [
                {name: '1. Lista de empleado', images: ['management_services/management_services.png'], description: 'La vista principal mostrara dos secciones, en l aparte izquierda esta el listado de empleados que estan en mi nivel organizacional, y en la parte izquierda estara el detalle del empelado que seleccione y los reportes que se podran ver.'},
                {name: '2. Filtros', images: ['management_services/filter_and_search.png'], description: 'Los filtros y busqueda se encuentrasn en la parte superior derecha de la seccion izquierda. Aca se puede filtar por cargos que estan ocupados, y por los cargos que actualmente esten vacantes. Tambien se puede realziar una busqueda detallada por el filtro que se seleccione.'},
                {name: '3. Ordenamiento', images: ['management_services/employee.png'], description: 'En algunas cabeceras de las tablas de consulta encontrarás un icono (imagen) flechas paralelas cruzadas, que te permitirán organizar la información con las fechas en orden ascendente o decente, también organizar algunos textos en orden alfabético. (Los campos que puedes organizar son los mismos que puedes filtrar).'},
                {name: '4. Paginación', images: ['management_services/report_employee.png'], description: 'En la parte inferior derecha de la tabla, encontrarás el total de los registros que existen en la consulta y las páginas en las que se encuentra distribuido.'},
              ]
            },
            {
              id: 7, name:'Gestor de contenidos', description: 'El gestor de contenidos se encarga de crear y mostrar noticias informativas en las siguientes secciones: 1. Noticias y envetos de bienestar, 2. Nomina, 3. Salud ocupacional y 4. Talento humano. Estas noticias pueden ser creadas publicamente para que cualquier empleado la pueda ver, o privada para que solo usted tenga acceso. Para acceder, encontraras el siguiente icono <img alt="image" width="70" height="28" class="img-sm" src="https://s3.amazonaws.com/hrsolutions/document_help/content/icono.png"> , en el menu del lado izquierdo del navegador.',
              steps: [
                {name: '1. Lista de noticias', images: ['content/content_2.png'], description: 'La vista principal mostrar todas los contenidos creados para le publico y si su usuario tiene permisos para crear, aparecera un boton redondo con un mas (+) en la parte inferior derecha.'}, 
                {name: '2. Boton de creación de contenido', image: ['content/content.png'], description: 'Cuando se ingresa por primera vez al gestor de contenidos y si no existe ningun contenido se mostrara una ventana pequeña al lado del boton de creacion, que te indica los pasos a seguir.'}, 
                {name: '3. Crear / Actualizar', images: ['content/create.png'], description: 'El formulario de creacion tiene unos pocos campos los cuales debes llenar y son los siguientes: <ul><li>Publico: Por defecto esta activo para que todos los empleados puedan ver tu contenido despues de creado, si lo que deseas es tener un contenido privado, se debe cambiar este estado.</li><li>Titulo: Es el titulo del contenido.</li><li>Categoria: Aca se debe seleccionar el tipo de contenido a crear.</li><li>Resumen: Una breve explicacion de lo que tratara el contenido (se aconseja que el texto sea descriptivo y corto).</li><li>Contenido: Esta campo es el mas completo, ya que desde aqui se crea y edita como se va a ver la informacion suministrada. Podra agragar amiganes y enlaces de video, como tambien colocar su propio estilo de letra y titulos.</li></ul>'}, 
                {name: '4. Publicación', images: ['content/publish.png'], description: 'Despues de la creacion del contenido, este estara disponible en el listado principal para luego abrir y ver. Aca se podra ver y comentar el contenido.'},
              ]
            },
            {
              id: 8, name:'Solicitudes', description: 'La seccion de <strong>solicitudes</strong> esta disponible para que usted pueda generar solicitudes de vacaciones y cesantias, registro de incapacidades, licencias, permisos y otras gestiones. Cada solicitud tiene un proceso de aprobación, que consta de 3 pasos: <strong>1.</strong> creación, <strong>2.</strong> aprobación del <i>"jefe de area"</i> (jefe directo), y <strong>3.</strong> aprobacion del departamenteo de <i>"recursos humanos"</i>. <p>Para acceder, encontraras el siguiente icono <img alt="image" width="70" height="28" class="img-sm" src="https://s3.amazonaws.com/hrsolutions/document_help/requests/icono.png"> , en el menu del lado izquierdo del navegador.</p>',
              steps: [
                {name: '1. Lista de solicitudes', images: ['requests/requests.png'], description: 'En la vista inicial se encuentra el listado de solicitudes creadas o pendientes, si no se encuentra ninguna, podemos ver el mensaje y boton "Ingresa a + Crear Solicitud", donde podemos darle click ahi o en el boton superior derecho para crear una nueva solictud. El paso 4, describe este proceso.'}, 
                {name: '2. Solicitudes pendientes', images: ['requests/slopes_list.png'], description: 'Como se menciono en el paso anterior, el primer listado que se observa es el de solicitudes pendientes. Si existen solicitudes creadas se puede ve que por cada solicitud se observa 3 circulos los cuales indican el estado en el que se encuentra.<ul><li>Circulo 1: Solicitud creada.</li><li>Circulo 2: Estado de aprobacion por Jefe Directo.</li><li>Circulo 3: Estado de aprobacion por Recursos Humanos.</li></ul><p>En este caso el primer circulo tiene un check, eso indica que nuestra solictud esta solamente creada y esta pendiente la aprobacion del jefe</p><p>En la solictud tambien se puede observar el tipo de solitud, el estado textual en el que se encuentra, y la fecha de creacion de la misma. Seguido a esto se puede encontrar un boton para ver el detalle de la solitud y si la solitud no ha sido aprobada por el jefe directo, podemos ver el boton de eliminar.</p>'}, 
                {name: '3. Solicitudes gestionadas', images: ['requests/managed_list.png'], description: 'En este listado de solicitudes gestionas, se puede ver todas las solicitudes pendientes, aprobadas y rechadas por el area de recursos humanos.<p>Si los dos circulos tienen check, significa que la solictud esta aprobada por el jefe directo y esta a la espera de la aprobación de recursos humanos, y si todos los circulos estan con check, significa que la solictud ha sido aprobada satisfactoriamente por la empresa, pasando por todos los filtros: jefe directo y recurso humanos. En caso contrario que algun circulo tenga una equis (x), esto significa que la solitud fue rechada en ese filtro.</p>'}, 
                {name: '4. Crear solicitud', images: ['requests/create_request.png', 'requests/create_request_2.png'], description: 'Al crear una solicitud se muestra un un select donde estan todos los tipos de solitud disponibles por la empresa. Se selecciona uno y enseguida nos aparece el formulario respectivo. Todos los campos que tienen un asterisco (*) al lado derecho son obligatorios. Despues de llenar el formulario le damos en el boton "Guardar cambios".'}, 
                {name: '5. Detalle de la solicitud', images: ['requests/detail_request.png'], description: 'El detalle de la solitud se obtiene dandole click en el boton "ver" de la solicitud alojada en algun listado. Aca se puede ver el estado, la fecha inicial y final de la solitud, el calculo de dias solicitados, el nombre y cargo del aprobador que sigue o en el que quedo (en caso de haber terminado el ciclo). Al lado derecho esta una linea de tiempo donde esta la fecha y observación.'},
              ]
            },
            {
              id: 9, name:'Aprobaciones', description: 'La seccion de aprovaciones estara disponible para usuarios con este privilegio (jefes de area y recursos humanos). Encontrara el listado de solicitudes que han peticionado sus personas a cargo. Para acceder, encontraras el siguiente icono <img alt="image" width="70" height="28" class="img-sm" src="https://s3.amazonaws.com/hrsolutions/document_help/approved/icono.png"> , en el menu del lado izquierdo del navegador.',
              steps: [
                {name: '1. Lista de solicitudes', images: ['approved/approved.png'], description: 'En esta seccion existen dos botones igual que en la seccion de "solicitudes", uno para el listado de solicitudes que han realizado los usuarios a cargo a nivel empresarial y que no han sido aprobadas o rechazadas, y el otro botn para ver todas las solicitudes que ya tienen respuesta.'},
                {name: '2. Busqueda de solicitudes', images: ['approved/search.png'], description: 'La busqueda de solicitudes esta disponible para realizarla por el nombre o cedula del usuario solicitante.'},
                {name: '3. Detalle de la solicitud', images: ['approved/detail.png'], description: 'En el listado se encuentra un boton "ver" por cada solicitud, y este nos muestra un detalle de la solicitud y adicional tiene una caja de texto para agregar un comentario y dos botones en la parte de abajo darle el estado de "Rechazar" o "Aprobar".'}
              ]
            }
          ];
        }]
      },
      onEnter: ['$rootScope','helpers_index',function($rootScope,helpers_index){
        $rootScope.helpers_list = helpers_index;
      }],
      onExit: ['$rootScope',function($rootScope){
        $rootScope.helpers_list = [];
      }],
      template: '<ui-view />'
    })
    .state('main.help.index', {
      url: "/all",
      data: {
        pageTitle: 'Ayuda',
        breadcrumb: {
          title:'NAVIGATION.HELP',
          list: [
          {
            url:'',
            name:'NAVIGATION.HELP',
            state:'active'
          }
          ]
        }
      },
      templateUrl: "views/help/index.html",
      onEnter: ['$rootScope',function($rootScope){
        
      }],
      onExit: ['$rootScope',function($rootScope){
        
      }]
      //controller: 'profileController'
    })
    //main.help.show
    .state('main.help.show', {
      url: "/view/:helpId",
      data: {
        pageTitle: 'Ayuda',
        breadcrumb: {
          title:'NAVIGATION.HELP_DETAIL',
          list: [
          {
            url: 'main.help.index',
            name: 'NAVIGATION.HELP',
            state: ''
          },
          {
            name: 'NAVIGATION.HELP_DETAIL',
            state:'active'
          }
          ]
        }
      },
      templateUrl: "views/help/show.html",
      onEnter: ['$rootScope','$stateParams','helper_show',function($rootScope,$stateParams,helper_show){
        
        $rootScope.help = helper_show[$stateParams.helpId-1];
      }],
      onExit: ['$rootScope',function($rootScope){
        
      }]
      //controller: 'profileController'
    })
    ;
  }]);
})();

