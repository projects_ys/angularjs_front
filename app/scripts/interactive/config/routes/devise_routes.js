(function(){
  'use strict';
  angular.module('Interactive')
  .config(['$stateProvider',function($stateProvider) {
    $stateProvider
    .state('devise', {
      abstract: true,
      templateUrl: "views/devise/content.html",
      controller: 'deviseController'
    })
    .state('devise.login', {
      url: "/login",
      templateUrl: "views/devise/login.html",
      data: { pageTitle: 'Iniciar sesión', specialClass: 'gray-bg', imgBackground: 'background_login' }
    })
    .state('devise.forgot_password',{
      url: "/forgot_password",
      templateUrl: "views/devise/forgot_password.html",
      data: { pageTitle: 'Olvide mi contraseña', specialClass: 'gray-bg', imgBackground: 'background_login' }
    })
    .state('lockscreen', {
      url: "/lockscreen",
      templateUrl: "views/devise/lockscreen.html",
      controller: 'deviseController',
      data: { pageTitle: 'Bloqueo de Pantalla', specialClass: 'gray-bg', imgBackground: 'background_login' }
    })
    .state('devise.password_edit',{
      url: "/password_edit/:token",
      templateUrl: "views/devise/password_edit.html",
      data: { pageTitle: 'Restablecer contraseña', specialClass: 'gray-bg', imgBackground: 'background_login' }
    });
  }]);
})();
