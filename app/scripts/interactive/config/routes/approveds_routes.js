(function(){
  'use strict';
  angular.module('Interactive')
  .config(['$stateProvider',function($stateProvider) {
    $stateProvider
    .state('main.approved',{
      abstract:true,
      url:'/approved',
      template: '<ui-view />',
      controller:'ApprovedsController'
    })
    .state('main.approved.index',{
      url:'/all',
      data: { pageTitle: 'NAVIGATION.APPROVED',
        breadcrumb: {
          title:'NAVIGATION.APPROVED',
          list: [
          {
            name: 'NAVIGATION.APPROVED',
            state:'active'
          }
          ],
          helpId: 8
        },
      },
      templateUrl:'views/approveds/index.html',
      onEnter: ['$rootScope','ApprovedRequest','$state','$timeout',function($rootScope,ApprovedRequest,$state,$timeout){
        if( $rootScope.$user.employee.is_first_approver === false  && $rootScope.$user.employee.is_second_approver === false ){
          $timeout(function(){ $state.go('error.permission'); }, 500);
        }
      }],
      onExit: ['$rootScope',function($rootScope){
      }]
    })
    .state('main.approved.show',{
      url:"/pending_detail/:request_id",
      data: { pageTitle: 'NAVIGATION.REQUEST_DETAIL',
        breadcrumb: {
          title:'NAVIGATION.REQUEST_DETAIL',
          list: [
          {
            url:'main.approved.index',
            name: 'NAVIGATION.APPROVED',
            state:''
          },
          {
            name: 'NAVIGATION.REQUEST_DETAIL',
            state:'active'
          }
          ],
          helpId: 8
        }
      },
      templateUrl:'views/approveds/detail.html',
      resolve:{
        request: ['ApprovedRequest','$stateParams',function(ApprovedRequest,$stateParams){
          return ApprovedRequest.get({ id: $stateParams.request_id}).$promise;
        }]
      },
      onEnter: ['$rootScope','$timeout','$state','$stateParams','request',function($rootScope,$timeout,$state,$stateParams,request){
        if($rootScope.$company.make_approvals){
          /**
           * @param object: json object [{id:'', name:'', date_begin:'', date_end:''}]
           * @param css_class: 'string'
           * @param color: 'string'
           **/
          var get_object_gantt = function(object, css_class, color){
            return {
              name: object.name,
              classes: css_class,
              content: '<i class="fa fa-user"></i> '+object.name,
              tasks: [
                {
                  name: object.name,
                  content: '<i class="fa fa-user"></i> <a ui-sref="main.approved.show({request_id:'+object.id+'})">{{task.model.name}}</a>',
                  color: color,
                  from: moment(object.date_begin),
                  to: moment(object.date_end),
                  progress: 100
                }
              ]
            };
          }

          $rootScope.request      = [];
          $rootScope.data_gantt   = [];
          $rootScope.total_days   = 0;
          $rootScope.column_width = 20;
          $rootScope.request      = request;
          $rootScope.request_id   = $stateParams.request_id;
          $rootScope.total_days   = moment(request.date_end).diff(moment(request.date_begin), 'days');
          $rootScope.data_gantt.push(get_object_gantt({
            id: request.id,
            name: request.employee.short_name,
            date_begin: request.date_begin,
            date_end: request.date_end
          }, 'color-primary', '#33cc33'));

          angular.forEach(request.applicants_crossed, function(value, key){
            $rootScope.total_days += moment(value.date_end).diff(moment(value.date_begin), 'days');

            $rootScope.data_gantt.push(get_object_gantt({
              id: value.id,
              name: value.employee.name,
              date_begin: value.date_begin,
              date_end: value.date_end
            }, '', '#F1C232'));
          });

          //columns width to gantt setting
          if ($rootScope.total_days <= 10) {
            $rootScope.column_width = 70;
          }else if ($rootScope.total_days <= 15) {
            $rootScope.column_width = 50;
          }

          $rootScope.view = 'show';
        }else{
          $timeout(function(){ $state.go('error.permission'); }, 100);
        }
      }],
      onExit: ['$rootScope',function($rootScope){
       $rootScope.request      = [];
       $rootScope.data_gantt   = [];
       $rootScope.total_days   = 0;
       $rootScope.column_width = 0;
       $rootScope.view         = '';
     }]
   });
  }]);
})();
