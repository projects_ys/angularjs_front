(function(){
  'use strict';
  angular.module('Interactive')
  .config(['$stateProvider',function($stateProvider) {
    $stateProvider
    .state('main.certificates', {
      abstract: true,
      url: "/certificates",
      templateUrl: 'views/certificate/index.html',
      resolve:{
        certificates:['Employee',function(Employee){
          return Employee.autoservice_files().$promise;
        }]
      },
      onEnter: ['$rootScope','certificates',function($rootScope,certificates){
        $rootScope.labor_certificates     = certificates['CLAB'] !== undefined ? certificates['CLAB'].data : []; //alpha
        $rootScope.income_certificates    = certificates['CIYR'] !== undefined ? certificates['CIYR'].data : []; //alpha
        $rootScope.vacations_certificates = certificates['VCTN'] !== undefined ? certificates['VCTN'].data : []; //date
        $rootScope.payroll_certificates   = certificates['VPAG'] !== undefined ? certificates['VPAG'].data : []; //date
      }],
      onExit: ['$rootScope', function($rootScope){
        $rootScope.labor_certificates     = [];
        $rootScope.vacations_certificates = [];
        $rootScope.payroll_certificates   = [];
        $rootScope.income_certificates    = [];
      }]
    })
    .state('main.certificates.labor', {
      url: '/labor/:id',
      data: { pageTitle: 'Certificado laboral', file : {type:'CLAB', state:'main.certificates.labor'},
        breadcrumb: {
          title:'NAVIGATION.AUTOSERVICE_LIST.LABOR',
          list: [
            {
              name: 'NAVIGATION.AUTOSERVICE',
              state:''
            },
            {
              url:'',
              name:'NAVIGATION.AUTOSERVICE_LIST.LABOR',
              state:'active'
            }
          ],
          helpId: 3
        }
      },
      resolve :{
        validate_permisson:['$rootScope','$state','$timeout',function($rootScope,$state,$timeout){
          return function () {
            if (  $rootScope.$company.show_certificates_labor === false   ) {
              $timeout(function(){ $state.go('error.permission'); }, 10);
            }
          }
       }]
      },
      onEnter: ['$rootScope','validate_permisson',function($rootScope,validate_permisson){
        validate_permisson();
        $rootScope.data_certificates = $rootScope.labor_certificates;
        $rootScope.order_by          = 'string_asc';
      }],
      onExit: ['$rootScope',function($rootScope){
        $rootScope.data_certificates = [];
        $rootScope.order_by          = '';
      }]
    })
    .state('main.certificates.vacations', {
      url: '/vacations/:id',
      data: { pageTitle: 'Carta de vacaciones', file : {type:'VCTN', state:'main.certificates.vacations'},
        breadcrumb: {
          title:'NAVIGATION.AUTOSERVICE_LIST.VACATION',
          list: [
          {
            name: 'NAVIGATION.AUTOSERVICE'
          },
          {
            url:'',
            name:'NAVIGATION.AUTOSERVICE_LIST.VACATION',
            state:'active'
          }
          ],
          helpId: 3
        }
      },
      resolve :{
        validate_permisson:['$rootScope','$state','$timeout',function($rootScope,$state,$timeout){
          return function () {
            if (  $rootScope.$company.show_certificates_vacations === false   ) {

              $timeout(function(){ $state.go('error.permission'); }, 10);
            }
          }
       }]
      },
      onEnter: ['$rootScope','validate_permisson',function($rootScope,validate_permisson){
        validate_permisson();
        $rootScope.data_certificates = $rootScope.vacations_certificates;
        $rootScope.order_by          = 'date';
      }],
      onExit: ['$rootScope',function($rootScope){
        $rootScope.data_certificates = [];
        $rootScope.order_by          = '';
      }]
    })
    .state('main.certificates.payroll', {
      url: '/payroll/:id',
      data: { pageTitle: 'Recibos de nomina', file : {type:'VPAG', state:'main.certificates.payroll'},
        breadcrumb: {
          title:'NAVIGATION.AUTOSERVICE_LIST.PAYROLL',
          list: [
          {
            name: 'NAVIGATION.AUTOSERVICE',
            state:''
          },
          {
            url:'',
            name: 'NAVIGATION.AUTOSERVICE_LIST.PAYROLL',
            state:'active'
          }
          ],
          helpId: 3
        }
      },
      resolve :{
        validate_permisson:['$rootScope','$state','$timeout',function($rootScope,$state,$timeout){
          return function () {
            if (  $rootScope.$company.show_certificates_payroll === false   ) {
              $timeout(function(){ $state.go('error.permission'); }, 10);
            }
          }
       }]
      },
      onEnter: ['$rootScope','validate_permisson',function($rootScope,validate_permisson){
        validate_permisson();
        $rootScope.data_certificates = $rootScope.payroll_certificates;
        $rootScope.order_by          = 'date';
      }],
      onExit: ['$rootScope',function($rootScope){
        $rootScope.data_certificates = [];
        $rootScope.order_by          = '';
      }]
    })
    .state('main.certificates.income', {
      url: '/income/:id',
      data: { pageTitle: 'Certificado de Ingresos y Retenciones', file : {type:'CIYR', state:'main.certificates.income'},
        breadcrumb: {
          title:'NAVIGATION.AUTOSERVICE_LIST.INCOME_BREADCRUMB',
          list: [
          {
            name: 'NAVIGATION.AUTOSERVICE',
            state:''
          },
          {
            url:'',
            name:'NAVIGATION.AUTOSERVICE_LIST.INCOME_BREADCRUMB',
            state:'active'
          }
          ],
          helpId: 3
        }
      },
      resolve :{
        validate_permisson:['$rootScope','$state','$timeout',function($rootScope,$state,$timeout){
          return function () {
            if (  $rootScope.$company.show_certificates_income === false   ) {
              $timeout(function(){ $state.go('error.permission'); }, 10);
            }
          }
       }]
      },
      onEnter: ['$rootScope','validate_permisson',function($rootScope,validate_permisson){
        validate_permisson();
        $rootScope.data_certificates = $rootScope.income_certificates;
        $rootScope.order_by          = 'string_desc';
      }],
      onExit: ['$rootScope',function($rootScope){
        $rootScope.data_certificates = [];
        $rootScope.order_by          = '';
      }]
    })
    }]);
  })();
