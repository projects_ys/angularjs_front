(function(){
  'use strict';
  angular.module('Interactive')
  .config(['$stateProvider',function($stateProvider) {
    $stateProvider
    .state('main.consultations', {
      abstract: true,
      url: "/consultations",
      templateUrl: 'views/consultations/index.html',
      resolve: {
        loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
          {
            serie: true,
            files: ['scripts/inspinia/plugins/dataTables/datatables.min.css']
          }
          ]);
        }]
      }
    })
    .state('main.consultations.income_and_deductions', {
      url: '/income_and_deductions',
      data: { pageTitle: 'Ingresos y Retenciones',
        srchttp: '/payments_deductions/by_employee/0',
        breadcrumb: {
          title:'NAVIGATION.CONSULTATION_LIST.INCOME_AND_DEDUCTIONS',
          list: [
          {
            name: 'NAVIGATION.CONSULTATION',
            state:''
          },
          {
            url:'',
            name:'NAVIGATION.CONSULTATION_LIST.INCOME_AND_DEDUCTIONS',
            state:'active'
          }
          ],
          helpId: 4
        }
      }
    })
    .state('main.consultations.payments_and_deductions', {
      url: '/payments_and_deductions',
      data: { pageTitle: 'Pagos y Deducciones',
        srchttp: '/indebtedness_level/by_employee/0',
        breadcrumb: {
          title:'NAVIGATION.CONSULTATION_LIST.PAYMENT_AND_DEDUCTIONS',
          list: [
          {
            name: 'NAVIGATION.CONSULTATION',
            state:''
          },
          {
            url:'',
            name:'NAVIGATION.CONSULTATION_LIST.PAYMENT_AND_DEDUCTIONS',
            state:'active'
          }
          ],
          helpId: 4
        }
      }
    })
    .state('main.consultations.seizures', {
      url: '/seizures',
      data: { pageTitle: 'Embargos', srchttp: '/embargo/by_employee/0',
        breadcrumb: {
          title:'NAVIGATION.CONSULTATION_LIST.SEIZURES',
          list: [
          {
            name: 'NAVIGATION.CONSULTATION',
            state:''
          },
          {
            url:'',
            name:'NAVIGATION.CONSULTATION_LIST.SEIZURES',
            state:'active'
          }
          ],
          helpId: 4
        }
      }
    })
    .state('main.consultations.loans', {
      url: '/loans',
      data: { pageTitle: 'Prestamos', srchttp: '/loan_records/by_employee/0',
        breadcrumb: {
          title:'NAVIGATION.CONSULTATION_LIST.LOANS',
          list: [
          {
            name: 'NAVIGATION.CONSULTATION',
            state:''
          },
          {
            url:'',
            name:'NAVIGATION.CONSULTATION_LIST.LOANS',
            state:'active'
          }
          ],
          helpId: 4
        }
      },
      resolve : {
        validate_permisson:['$rootScope','$state','$timeout',function($rootScope,$state,$timeout){
          return function () {
            if ( $rootScope.$company.show_loans === null || angular.equals($rootScope.$company.show_loans, false)  ) {
              $timeout(function(){ $state.go('error.permission'); }, 10);
            }
          }
       }]
     },
     onEnter : ['validate_permisson',function(validate_permisson){
       validate_permisson();
     }]
    })
    .state('main.consultations.enjoyed_vacations', {
      url: '/enjoyed_vacations',
      data: { pageTitle: 'Vacaciones disfrutadas', srchttp: '/vacation_record/by_employee/0',
        breadcrumb: {
          title:'NAVIGATION.CONSULTATION_LIST.ENJOYED_VACATIONS',
          list: [
          {
            name: 'NAVIGATION.CONSULTATION',
            state:''
          },
          {
            url:'',
            name:'NAVIGATION.CONSULTATION_LIST.ENJOYED_VACATIONS',
            state:'active'
          }
          ],
          helpId: 4
        }
      },
      resolve : {
        validate_permisson:['$rootScope','$state','$timeout',function($rootScope,$state,$timeout){
          return function () {
            if ( $rootScope.$company.show_vacations === null || $rootScope.$company.show_vacations === false  ) {
              $timeout(function(){ $state.go('error.permission'); }, 10);
            }
          }
       }]
     },
     onEnter : ['validate_permisson',function(validate_permisson){
       validate_permisson();
     }]
    })
    .state('main.consultations.compensatory_vacations', {
      url: '/compensatory_vacations',
      data: { pageTitle: 'Vacaciones compensadas', srchttp: '/compensatory_vacation_records/by_employee/0',
        breadcrumb: {
          title:'NAVIGATION.CONSULTATION_LIST.COMPENSATORY_VACATIONS',
          list: [
          {
            name: 'NAVIGATION.CONSULTATION',
            state:''
          },
          {
            url:'',
            name:'NAVIGATION.CONSULTATION_LIST.COMPENSATORY_VACATIONS',
            state:'active'
          }
          ],
          helpId: 4
        }
      },
      resolve : {
        validate_permisson:['$rootScope','$state','$timeout',function($rootScope,$state,$timeout){
          return function () {
            if ( $rootScope.$company.show_vacations_c === null || angular.equals($rootScope.$company.show_vacations_c, false)  ) {
              $timeout(function(){ $state.go('error.permission'); }, 10);
            }
          }
       }]
     },
     onEnter : ['validate_permisson',function(validate_permisson){
       validate_permisson();
     }]
    })
    .state('main.consultations.balance_vacations', {
      url: '/balance_vacations',
      data: { pageTitle: 'Saldo de vacaciones', srchttp: '/vacation_balance_records/by_employee/0',
        breadcrumb: {
          title:'NAVIGATION.CONSULTATION_LIST.BALANCE_VACATIONS',
          list: [
          {
            name: 'NAVIGATION.CONSULTATION',
            state:''
          },
          {
            url:'',
            name:'NAVIGATION.CONSULTATION_LIST.BALANCE_VACATIONS',
            state:'active'
          }
          ],
          helpId: 4
        }
      }
    })
    .state('main.consultations.permissions', {
      url: '/permissions',
      data: { pageTitle: 'Permisos', srchttp: '/permissions/by_employee/0',
        breadcrumb: {
          title:'NAVIGATION.CONSULTATION_LIST.PERMISSIONS',
          list: [
          {
            name: 'NAVIGATION.CONSULTATION',
            state:''
          },
          {
            url:'',
            name:'NAVIGATION.CONSULTATION_LIST.PERMISSIONS',
            state:'active'
          }
          ],
          helpId: 4
        }
      },
      resolve : {
        validate_permisson:['$rootScope','$state','$timeout',function($rootScope,$state,$timeout){
          return function () {
            if ( $rootScope.$company.show_licenses === null || angular.equals($rootScope.$company.show_licenses, false)  ) {
              $timeout(function(){ $state.go('error.permission'); }, 10);
            }
          }
       }]
     },
     onEnter : ['validate_permisson',function(validate_permisson){
       validate_permisson();
     }]
    })
    .state('main.consultations.incapacities', {
      url: '/incapacities',
      data: { pageTitle: 'Incapacidades', srchttp: '/incapacities/by_employee/0',
        breadcrumb: {
          title:'NAVIGATION.CONSULTATION_LIST.INCAPACITIES',
          list: [
          {
            name: 'NAVIGATION.CONSULTATION',
            state:''
          },
          {
            url:'',
            name:'NAVIGATION.CONSULTATION_LIST.INCAPACITIES',
            state:'active'
          }
          ],
          helpId: 4
        }
      },
      resolve : {
        validate_permisson:['$rootScope','$state','$timeout',function($rootScope,$state,$timeout){
          return function () {
            if ( $rootScope.$company.show_inhabilities === null || angular.equals($rootScope.$company.show_inhabilities, false)  ) {
              $timeout(function(){ $state.go('error.permission'); }, 10);
            }
          }
       }]
     },
     onEnter : ['validate_permisson',function(validate_permisson){
       validate_permisson();
     }]
    })
    ;
    }]);
  })();
