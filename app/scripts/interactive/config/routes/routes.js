(function(){
  'use strict';
  angular.module('Interactive')
  .config(['$stateProvider','$urlRouterProvider',function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/login");
    $stateProvider
    .state('error', {
      abstract: true,
      templateUrl: "views/errors/content.html"
    })
    .state('error.500', {
      url: "/error_500",
      templateUrl: 'views/errors/error_500.html',
      data: { pageTitle: '500', specialClass: 'gray-bg' }
    })
    .state('error.permission', {
      url:"/error_permission",
      templateUrl: 'views/errors/error_not_permission.html',
      data: {  specialClass: 'gray-bg'}
    })
    .state('main', {
      abstract: true,
      templateUrl: "views/common/content.html",
      controller: 'mainController',
      resolve: {
        auth: ['$auth','$state',function($auth,$state) {
          return $auth.validateUser().catch(function(err){
            console.info('not authenticated',err);
            $state.go('devise.login');
          });
        }]
      },
    })
    .state('main.dashboard', {
      url: "/home",
      templateUrl: "views/dashboard/index.html",
      data: {
        imgBackground: '',
        pageTitle: 'Inicio',
        breadcrumb: {
          title:'NAVIGATION.DASHBOARD',
          list: [],
          helpId: 1
        }
      },
      controller: 'dashboardController',
      resolve: {
        loadPlugin: ['$ocLazyLoad',function ($ocLazyLoad) {
          return $ocLazyLoad.load([
          {
            name: 'ui.sortable',
            files: ['scripts/inspinia/plugins/ui-sortable/sortable.js']
          },
          {
            name: 'angles',
            files: ['scripts/inspinia/plugins/chartJs/angles.js', 'scripts/inspinia/plugins/chartJs/Chart.min.js']
          },
          {
            name: 'angular-peity',
            files: ['scripts/inspinia/plugins/peity/jquery.peity.min.js', 'scripts/inspinia/plugins/peity/angular-peity.js']
          },
          {
            name: 'ui.checkbox',
            files: ['scripts/inspinia/plugins/bootstrap/angular-bootstrap-checkbox.js']
          },
          {
            serie: true,
            name: 'angular-flot',
            files: ['scripts/inspinia/plugins/flot/jquery.flot.js', 'scripts/inspinia/plugins/flot/jquery.flot.time.js', 'scripts/inspinia/plugins/flot/jquery.flot.tooltip.min.js', 'scripts/inspinia/plugins/flot/jquery.flot.spline.js', 'scripts/inspinia/plugins/flot/jquery.flot.resize.js', 'scripts/inspinia/plugins/flot/jquery.flot.pie.js', 'scripts/inspinia/plugins/flot/curvedLines.js', 'scripts/inspinia/plugins/flot/angular-flot.js']
          }
          ]);
        }]
      }
    })
    .state('main.profile', {
      url: "/profile",
      data: {
        pageTitle: 'Perfíl',
        breadcrumb: {
          title:'NAVIGATION.HEADER.PROFILE',
          list: [
          {
            url:'',
            name:'NAVIGATION.HEADER.PROFILE',
            state:'active'
          }
          ],
          helpId: 2
        }
      },
      templateUrl: "views/user/profile.html",
      resolve:{
        data:['$rootScope','$http',function($rootScope,$http){
          return $http.get($rootScope.url_request('/employees/profile.json'))
          .then(function(response,status) {
            return response.data;
          },function(response,status){
            return response;
          });
        }]
      },
      controller: 'profileController'
    })
    .state('main.manager_services', {
      url: "/management_services",
      data: { pageTitle: 'Servicios gerenciales',
        breadcrumb: {
          title:'NAVIGATION.MANAGER_SERVICE',
          templateUrl: 'views/manager_services/index.html',
          list: [
            {
              name: 'NAVIGATION.MANAGER_SERVICE',
              state:'active'
            }
          ],
          helpId: 5
        }
      },
      controller: 'ManagerServiceController',
      templateUrl: 'views/manager_services/index.html',
      resolve: {
        loadPlugin:['$ocLazyLoad',function ($ocLazyLoad) {
          return $ocLazyLoad.load([
          {
            serie: true,
            files: ['scripts/inspinia/plugins/dataTables/datatables.min.css']
          }
          ]);
        }],
        validate_permisson:['$rootScope','$state','$timeout',function($rootScope,$state,$timeout){
          return function () {
            if ( $rootScope.$company.show_services_management === null || angular.equals($rootScope.$company.show_services_management, false) || angular.equals($rootScope.user.employee.see_rpgen, false) ) {
              $timeout(function(){ $state.go('error.permission'); }, 10);
            }
          }
       }]
     },
     onEnter: ['validate_permisson',function(validate_permisson){
       //validate_permisson();
     }]
    })
    .state('main.orgchart', {
      url: '/orgchart/organigrama',
      templateUrl: "views/orgchart/organigrama.html",
      controller: "orgchartController",
      data: { pageTitle: 'Posición Organizativa',
        breadcrumb: {
          title:'NAVIGATION.ORGCHART',
          templateUrl: 'views/orgchart/organigrama.html',
          list: [
            {
              name: 'NAVIGATION.ORGCHART',
              state:'active'
            }
          ],
          helpId: 5
        }
      },
      resolve : {
        validate_permisson:['$rootScope','$state','$timeout',function($rootScope,$state,$timeout){
          return function () {
            if (  $rootScope.$company.show_organizate_chart === null || angular.equals($rootScope.$company.show_organizate_chart, false) || angular.equals($rootScope.$user.employee.see_organ, 'false') ) {
            $timeout(function(){ $state.go('error.permission'); }, 10);
            }
          }
       }],
       organigram :['$rootScope','$http',function($rootScope,$http){
         return function (url) {
           return $http.get(url);
         }
       }]
     },
     onEnter: ['validate_permisson',function(validate_permisson){
       validate_permisson();
     }]
    })
    ;
  }]);
})();
