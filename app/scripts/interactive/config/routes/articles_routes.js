//  articles
// create 31-10-2016
// modify route
(function(){
  'use strict';
  angular.module('Interactive')
  .config(['$stateProvider',function($stateProvider) {
    $stateProvider
    .state('main.content',{
      abstract:true,
      url:'/content',
      resolve:{
        categories:['CategoryArticle',function(CategoryArticle){
          return CategoryArticle.get();
        }],
        validate_permisson:['$rootScope','$state','$timeout',function($rootScope,$state,$timeout){
          return function () {

            if ( !$rootScope.$company.show_articles) {
              $timeout(function(){ $state.go('error.permission'); }, 10);
            }
          }
       }]
      },
      onEnter: ['$rootScope','categories','validate_permisson',function($rootScope,categories,validate_permisson){
        validate_permisson();
        $rootScope.categoriesContent=[];
        angular.forEach(categories, function(value, key){
           $rootScope.categoriesContent.push({ name: value.value, value: value.id , group: "categorias", category:value.identifier });
        });
      }],
      controller:'ArticlesController',
      template: '<ui-view />'
    })
    .state('main.content.all',{
      url:'/all',
      data: { pageTitle: 'Gestor de contenidos',
        breadcrumb: {
          title:'NAVIGATION.CONTENT_MANAGER',
          list: [
          {
            name: 'NAVIGATION.CONTENT_MANAGER',
            state:'active'
          }
          ],
          helpId: 6
        },
      },
      templateUrl:'views/articles/index.html',
      onEnter: ['$rootScope','Article',function($rootScope,Article){
        $rootScope.show_loading=true;
        $rootScope.articles=Article.query();
          $rootScope.articles.$promise.then(function (data) {
              $rootScope.show_loading=false;
          })
      }],
      onExit: ['$rootScope',function($rootScope){
        $rootScope.articles = Array.new;
        $rootScope.show_loading=false;
      }]
    })
    .state('main.content.new',{
      url:'/new',
      data: { pageTitle: 'Nuevo Contenido',
        breadcrumb: {
          title:'NAVIGATION.CONTENT_MANAGER_NEW',
          list: [
          {
            url:'main.content.all',
            name: 'NAVIGATION.CONTENT_MANAGER',
            state:''
          },
          {
            name: 'NAVIGATION.CONTENT_MANAGER_NEW',
            state:'active'
          }
          ],
          helpId: 6
        }
      },
      templateUrl:'views/articles/new.html',
      resolve: {
        validate_new_cont:['$rootScope','$state','$timeout',function($rootScope,$state,$timeout){
          return function () {
            if ( (!$rootScope.$company.show_articles) || ( $rootScope.$user.employee.new_cont == 'false' ) ) {
              $timeout(function(){ $state.go('error.permission'); }, 10);
            }
          }
       }],
        loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
          {
            serie: true,
            files: ['scripts/vendor/summernote/lang/summernote-es-ES.js']
          }
          ]);
        }
        ]
      },
      onEnter: ['$rootScope','validate_new_cont',function($rootScope,validate_new_cont){
        validate_new_cont();
      }],
      onExit: ['$rootScope',function($rootScope){
        $rootScope.model_article = Array.new;
      }]
    })
    .state('main.content.edit',{
      url:"/edit/:articleId",
      data: { pageTitle: 'Editar Contenido',
        breadcrumb: {
          title:'NAVIGATION.CONTENT_MANAGER_EDIT',
          list: [
          {
            url:'main.content.all',
            name: 'NAVIGATION.CONTENT_MANAGER',
            state:''
          },
          {
            name: 'NAVIGATION.CONTENT_MANAGER_EDIT',
            state:'active'
          }
          ],
          helpId: 6
        }
      },
      templateUrl:'views/articles/edit.html',
      resolve: {
        loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
          {
            serie: true,
            files: ['scripts/vendor/summernote/lang/summernote-es-ES.js']
          }
          ]);
        }
        ]
      },
      onEnter: ['$rootScope','Article','$stateParams',function($rootScope,Article,$stateParams){
        // asigna al modelo
        $rootScope.model_article = Article.get({id:$stateParams.articleId});
        //  promise para asignar categoria a group select
        $rootScope.model_article.$promise.then(function (response) {
          $rootScope.categoriesContent.forEach( function(value) {
              if (response.my_category.identifier === value['category'] ) {
                 $rootScope.model_article.categoria = value;
               }
          });
        });
      }],
      onExit: ['$rootScope',function($rootScope){
          $rootScope.model_article            = Array.new;
      }]
    })
    .state('main.content.view',{
      url:"/view/:articleId",
      data: { pageTitle: 'Detalle',
        breadcrumb: {
          title:'NAVIGATION.CONTENT_MANAGER_DETAIL',
          list: [
          {
            url:'main.content.all',
            name: 'NAVIGATION.CONTENT_MANAGER',
            state:''
          },
          {
            name: 'NAVIGATION.CONTENT_MANAGER_DETAIL',
            state:'active'
          }
          ],
          helpId: 6
        }
      },
      resolve: {
        viewed:['$rootScope','$stateParams','Article',function ($rootScope,$stateParams,Article) {
            return Article.viewed({id:$stateParams.articleId}).$promise;
        }]
      },
      onEnter: ['$rootScope','Article', '$stateParams', function($rootScope, Article, $stateParams){
          $rootScope.show_loading=true;
         $rootScope.article = Article.get({id:$stateParams.articleId});
         $rootScope.article.$promise.then(function (data) {
           $rootScope.show_loading=false;
         })

      }],
      onExit: ['$rootScope',function($rootScope){
         $rootScope.article = null;
         $rootScope.show_loading=false;
      }],
      templateUrl:'views/articles/view.html'
    })

  }]);
})();
