(function(){
  'use strict';
  //Rutas
  PDFJS.workerSrc = 'scripts/vendor/pdfjs-dist/build/pdf.worker.js';

  // Informacion de entorno
  var runEnv = {
    isDevelopment: window.location.port === '9000',
    isDevelopmentCloud: window.location.host.split('.')[0].indexOf("demo") !== -1,
    dummyHost: 'hrsolutions.co',
    clientHostResourceUrl : 'http://hrsolutions.amazonaws.com/client-resources/'
  };
  // Configuracion API HR
  var hrapi = {};
  hrapi.hostname = function(){
    if(runEnv.isDevelopment){
      //pre-staging
     //  return 'http://develoment.hrinteractive.co';
     return 'http://staging-services.hrinteractive.co';
    }else if (runEnv.isDevelopmentCloud) {
      //staging
      return 'http://staging-services.hrinteractive.co';
    }
    //production
    return 'https://services.hrinteractive.co';
  }();
  hrapi.apiName = 'humanresources';
  hrapi.apiVersion = 'v2';
  hrapi.apiBaseUrl = function(path){
    return hrapi.hostname + '/api/' + hrapi.apiVersion + path;
  };

  hrapi.baseUrl = function(path){
    if(runEnv.isDevelopment === true){
      return hrapi.hostname + path;
    }
    return path
  };

  hrapi.labor_certificate_id = '0001';
  hrapi.translate_file_location = function(){
    var prefix = 'https://s3.amazonaws.com/hrsolutions/locales/';
    if(runEnv.isDevelopment === true){
      prefix = 'scripts/vendor/locales/'
    }
    return prefix;
  };
  hrapi.current_domain = window.location.host.split('.')[0];

  var constants = {}
  constants.labors_key    = 'CLAB';
  constants.vacations_key = 'VCTN';
  constants.payrolls_key  = 'VPAG';
  constants.incomes_key   = 'CIYR';

  angular.module('interactive.config', [])
  .constant('RUNENV_CONF', runEnv)
  .constant('HRAPI_CONF', hrapi)
  .constant('CERTIFICATES', constants)
  .constant("TYPES_REQUESTS", {
    "PENDING_APPROVE_BOSS": "ReqEmpStaPenAproJef", //Pendiente Aprobacion JEFE
    "CANCEL_APPROVE_BOSS": "ReqEmpStaCanJef",  //Cancelado JEFE
    "PENDING_APPROVE_RH": "ReqEmpStaPenRH",   //Pendiente RH
    "CANCEL_APPROVE_RH": "ReqEmpStaCanRH",     //Cancelado RH
    "APPROVE_RH": "ReqEmpStaAprRH"            //Aprobado RH
  });
})();
