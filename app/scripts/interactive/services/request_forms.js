(function(){
  'use strict';
  angular.module('Interactive')
  .service('requestForms', [function() {
    var date_picker_config = {
      dateinit:  moment(new Date()),
      startview: "day",
      minview:   "day",
      format:    "YYYY-MM-DD"
    };

    var date_time_picker_config = {
      dateinit:  moment(new Date()),
      startview: "day",
      minview:   "hour",
      format:    "YYYY-MM-DD HH:mm"
    };

    var basic_fields = [
    {
      className: 'row',
      fieldGroup: [
      {
        className: 'col-sm-5',
        key: 'date_begin',
        type: 'date_picker',
        templateOptions: {
          label: 'Fecha Inicial',
          required: true
        },
        controller: ['$scope',function($scope) {
          $scope.data = date_picker_config;
        }]
      },
      {
        className: 'col-sm-5',
        key: 'date_end',
        type: 'date_picker',
        templateOptions: {
          label: 'Fecha Final',
          required: true
        },
        controller: ['$scope',function($scope) {
          $scope.data = date_picker_config;
        }]
      },
      {
        className: 'col-sm-2',
        key: 'days_between_dates',
        type: 'input_number',
        templateOptions: {
          label: 'Total Dias'
        },
        controller: ['$scope','$translate',function($scope,$translate){
          var a_translate = [
          'FORMLY_MESSAGES.MIN_DAYS'
          ];
          $translate(a_translate).then(function (translations) {
            $scope.data = {
              required: true,
              disabled: true,
              min: 1,
              message_min: translations['FORMLY_MESSAGES.MIN_DAYS']
            }
          });

          var get_diff = function(value1, value2){
            $scope.date_ini = moment(new Date(value1.split("-")));
            $scope.date_fin = moment(new Date(value2.split("-")));

            var diff_days = parseInt($scope.date_fin.diff($scope.date_ini, 'days'));
            return diff_days >= 0 ? diff_days+1 : diff_days;
          }

          $scope.$watch('model.date_begin', function(value){
            if((value !== '' && value !== undefined) && $scope.model.date_end !== ''){
              $scope.model.days_between_dates = get_diff(value, $scope.model.date_end);
            }
          });

          $scope.$watch('model.date_end', function(value){
            if((value !== '' && value !== undefined) && $scope.model.date_begin !== ''){
              $scope.model.days_between_dates = get_diff($scope.model.date_begin, value);
            }
          });
        }]
      }
      ]
    },
    {
      className: 'col-sm-12',
      key: 'observation_request',
      type: 'textarea',
      templateOptions: {
        label: 'Observaciones'
      }
    }
    ];

    var available_days = function(){
      return [
      {
        className: 'row',
        fieldGroup: [
          {
            className: "col-xs-6",
            type: "widget1",
            key: "available_contingent",
            templateOptions: {
              label: "Dias disponibles",
            }
          },
          {
            className: "col-xs-6",
            type: "input_number",
            key: "days_request",
            templateOptions: {
              label: "Dias solicitados"
            },
            controller: ['$scope','$translate',function($scope,$translate){
              var a_translate = [
              'FORMLY_MESSAGES.RANGE_DATE'
              ];
              $translate(a_translate).then(function (translations) {
                $scope.data = {
                  required: true,
                  disabled: false,
                  min: 1,
                  max: $scope.model.available_contingent,
                  message_min: translations['FORMLY_MESSAGES.RANGE_DATE'],
                  message_max: translations['FORMLY_MESSAGES.RANGE_DATE']
                }
              });
            }]
          },
          {
            className: 'col-sm-12',
            key: 'observation_request',
            type: 'textarea',
            templateOptions: {
              label: 'Observaciones'
            }
          }
        ]
      }
      ];
    }

    var permisions_only_date_form = function(){
      return [
      {
        className: 'row',
        fieldGroup: [
        {
          className: 'col-sm-4',
          key: 'date_begin',
          type: 'date_picker',
          templateOptions: {
            label: 'Fecha Inicio',
            required: true
          },
          controller: ['$scope',function($scope) {
            $scope.data = date_picker_config;
          }]
        },
        {
          className: 'col-sm-3',
          key: 'available_days',
          type: 'input_number',
          templateOptions: {
            label: "Dias Disponibles"
          },
          controller: ['$scope','$translate',function($scope,$translate){
            var a_translate = [
            'FORMLY_MESSAGES.MIN_DAYS'
            ];
            $translate(a_translate).then(function (translations) {
              $scope.data = {
                required: true,
                disabled: true,
                min: 0,
                message_min: translations['FORMLY_MESSAGES.MIN_DAYS']
              }
            });

          }]
        },
        {
          className: 'col-sm-5',
          key: 'date_return',
          type: 'text_date_return_input',
          templateOptions: {
            label: 'Fecha de Regreso'
          },
          controller: ['$scope','$translate',function($scope,$translate){
            var a_translate = [
              'FORMLY_MESSAGES.REQUIRED_DATE_BEGIN'
            ];
            $translate(a_translate).then(function (translations) {
              $scope.data = {
                required: true,
                disabled: true,
                message: translations['FORMLY_MESSAGES.REQUIRED_DATE_BEGIN']
              }
            });
          }]
        }
        ]
      },
      {
        className: 'col-sm-12',
        key: 'observation_request',
        type: 'textarea',
        templateOptions: {
          label: 'Observaciones'
        }
      },
      {
        className:"col-lg-12",
        key:"file_support",
        type:"file_request_input",
        templateOptions: {
          label: 'Adjuntar Archivo'
        },
        controller: ['$scope','$translate',function($scope,$translate){
          $scope.permission_file=[".gif", ".png", ".jpeg", ".jpg", ".doc", ".pdf",".docx",".xls",".xlsx"];
          $scope.filesizeMax="5000000";
          var a_translate = [
            'FORMLY_MESSAGES.REQUIRED',
            'FORMLY_MESSAGES.FILE_EXT_INVALID',
            'FORMLY_MESSAGES.FILE_SIZE_INVALID',
            'REQUEST.FILE_UPLOAD'
          ];
          $translate(a_translate).then(function (translations) {
            $scope.description=translations['REQUEST.FILE_UPLOAD'];
            $scope.msg_error = {
              required          : translations['FORMLY_MESSAGES.REQUIRED'],
              file_size_invalid : translations['FORMLY_MESSAGES.FILE_SIZE_INVALID'],
              file_ext_invalid  : translations['FORMLY_MESSAGES.FILE_EXT_INVALID']
            }
          });
        }]
      }
      ];
    }

    var permisions_form = function(){
      return [
      {
        className: 'row',
        fieldGroup: [
          {
            className: 'col-sm-6',
            key: 'date_begin',
            type: 'date_picker',
            templateOptions: {
              label: 'Fecha y Hora Inicial',
              required: true
            },
            controller: ['$scope',function($scope) {
              $scope.data = date_time_picker_config;
            }]
          },
          {
            className: 'col-sm-6',
            key: 'date_end',
            type: 'date_picker',
            templateOptions: {
              label: 'Fecha y Hora Final',
              required: true
            },
            controller: ['$scope',function($scope) {
              $scope.data = date_time_picker_config;
            }]
          },
          {
            className: 'col-sm-6',
            key: 'days_between_dates',
            type: 'input_number_dates',
            templateOptions: {
              label: "Total Dias Solicitados"
            },
            controller: ['$scope','$translate',function($scope,$translate){
              var a_translate = [
                'FORMLY_MESSAGES.MIN_DAYS'
              ];
              $translate(a_translate).then(function (translations) {
                $scope.data = {
                  required: true,
                  disabled: true,
                  min: 0,
                  message_min: translations['FORMLY_MESSAGES.MIN_DAYS']
                }
              });
            }]
          },
          {
            className: 'col-sm-6',
            key: 'hours_between_dates',
            type: 'input_number',
            templateOptions: {
              label: 'Total Horas Solicitadas'
            },
            controller: ['$scope','$translate',function($scope,$translate){
              var a_translate = [
                'FORMLY_MESSAGES.MIN_HOURS'
              ];
              $translate(a_translate).then(function (translations) {
                $scope.data = {
                  required: true,
                  disabled: true,
                  min: 1,
                  message_min: translations['FORMLY_MESSAGES.MIN_HOURS']
                }
              });
            }]
          },
          {
            className: 'col-sm-12',
            key: 'observation_request',
            type: 'textarea',
            templateOptions: {
              label: 'Observaciones'
            }
          },
          {
            className:"col-lg-12",
            key:"file_support",
            type:"file_request_input",
            templateOptions: {
              label: 'Adjuntar Archivo'
            },
            controller: ['$scope','$translate',function($scope,$translate){
              $scope.permission_file=[".gif", ".png", ".jpeg", ".jpg", ".doc", ".pdf",".docx",".xls",".xlsx"];
              $scope.filesizeMax="5000000";
              var a_translate = [
              'FORMLY_MESSAGES.REQUIRED',
              'FORMLY_MESSAGES.FILE_EXT_INVALID',
              'FORMLY_MESSAGES.FILE_SIZE_INVALID',
              'REQUEST.FILE_UPLOAD'
              ];
              $translate(a_translate).then(function (translations) {
                $scope.description=translations['REQUEST.FILE_UPLOAD'];
                $scope.msg_error = {
                  required          : translations['FORMLY_MESSAGES.REQUIRED'],
                  file_size_invalid : translations['FORMLY_MESSAGES.FILE_SIZE_INVALID'],
                  file_ext_invalid  : translations['FORMLY_MESSAGES.FILE_EXT_INVALID']
                }
              });
            }]
          }
        ]
      }];
    }


    this.get_form = function(value, available_contingent){
      var type = value.substring(0, 5);
      var form = [];
      if (type === "CESA_") { //Cesantias
      }else if (type === "INCA_") { //Incapacities
      }else if (type === "PERM_" && available_contingent !== null) { //Licenses with only datepicker initial
        form = permisions_only_date_form();
      }else if (type === "PERM_" && available_contingent === null) { //Other Licenses and Permissions
        form = permisions_form();
      }else if (type === "VACA_") { //Vacations
        form = basic_fields;
      }else if (type === "VCCP_") { //Compensation Vacations
        form = available_days();
      }
      return form;
    };

  }]);
})();
