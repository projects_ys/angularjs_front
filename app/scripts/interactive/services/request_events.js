(function(){
  'use strict';
  angular.module('Interactive')
  .service('requestEvents', ['$rootScope','$state','swalService','Request','TYPES_REQUESTS',function($rootScope,$state,swalService,Request,TYPES_REQUESTS) {
    var request_icon_states = function(state){
      switch(state){
        case TYPES_REQUESTS.PENDING_APPROVE_BOSS: return ['fa fa-check','fa fa-question fa-lg','fa fa-question fa-lg'];
        case TYPES_REQUESTS.CANCEL_APPROVE_BOSS: return ['fa fa-check','fa fa-close','fa fa-close'];
        case TYPES_REQUESTS.PENDING_APPROVE_RH: return ['fa fa-check','fa fa-check','fa fa-question fa-lg '];
        case TYPES_REQUESTS.CANCEL_APPROVE_RH: return ['fa fa-check','fa fa-check','fa fa-close'];
        case TYPES_REQUESTS.APPROVE_RH: return ['fa fa-check','fa fa-check','fa fa-check'];
      }
    };
		var request_btn_states = function(state){
      switch(state){
        case TYPES_REQUESTS.PENDING_APPROVE_BOSS: return ['btn btn-primary btn-sm btn-circle','btn btn-info btn-outline btn-circle gray-bg','btn btn-info btn-outline btn-circle gray-bg'];
        case TYPES_REQUESTS.CANCEL_APPROVE_BOSS: return ['btn btn-primary btn-sm btn-circle','btn btn-danger btn-sm btn-circle','btn btn-danger btn-sm btn-circle'];
        case TYPES_REQUESTS.PENDING_APPROVE_RH: return ['btn btn-primary btn-sm btn-circle','btn btn-primary btn-sm btn-circle','btn btn-info btn-outline btn-circle gray-bg'];
        case TYPES_REQUESTS.CANCEL_APPROVE_RH: return ['btn btn-primary btn-sm btn-circle','btn btn-primary btn-sm btn-circle','btn btn-danger btn-sm btn-circle'];
        case TYPES_REQUESTS.APPROVE_RH: return ['btn btn-primary btn-sm btn-circle','btn btn-primary btn-sm btn-circle','btn btn-primary btn-sm btn-circle'];
      }
    };
    var delete_request = function (idDelete) {
      swalService.custonConfirm('REQUESTS.DELETE.TITLE','DELETE_MESSAGE','warning','DELETE_CANCEL_BTN_TEXT','DELETE_CONFIRM_BTN_TEXT',
        function () { // en caso  confirm is positive true
          Request.remove({id: idDelete}, function(response){
            swalService.sampleMessage('REQUESTS.TITLE','DELETE_CONFIRM_MESSAGE','info');
            if($rootScope.view === 'index'){
              $state.reload();
            }else if($rootScope.view === 'show'){
              $state.transitionTo('main.request.index');
            }
          }, function(error){
            swalService.error400(error.status,error.data);
          });
        },
        function () { // en caso  cancel confirm  false
          swalService.sampleMessage('CANCEL_TITLE','REQUESTS.CANCEL.MESSAGE','info');
        }
      );
    }

    return {
      request_icon_states: request_icon_states,
      delete_request      : delete_request,
      request_btn_states:  request_btn_states
    };
  }]);
})();
