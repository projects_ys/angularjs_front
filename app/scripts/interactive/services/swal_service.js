  (function(){
  'use strict';
  angular.module('Interactive')
  .service('swalService', ['SweetAlert','$rootScope','$translate','notify',function(SweetAlert,$rootScope,$translate,notify) {
  // parametros de configuracion SweetAlert
    this.settings={
      title:null,
      text:null,
      type:null,
      showCancelButton: false,
      confirmButtonColor:$rootScope.$company.primary_color,
      confirmButtonText:$translate.instant('SWEAT_ALERTS.BTN_OK'),
      cancelButtonText: $translate.instant('SWEAT_ALERTS.CANCEL_TITLE'),
      closeOnConfirm: false,
      closeOnCancel: false
    };
    //  Mensajes de Confirmacion Aplicando Diccionario translate
    this.custonConfirm = function (titles,texts,types,textCancel,textConfirm,confirmfns,cancelfns) {
         this.settings.title =$translate.instant('SWEAT_ALERTS.'+titles);
         this.settings.text = $translate.instant('SWEAT_ALERTS.'+texts);
         this.settings.type = types;
         this.settings.cancelButtonText = $translate.instant('SWEAT_ALERTS.'+textCancel);
         this.settings.confirmButtonText = $translate.instant('SWEAT_ALERTS.'+textConfirm);
         this.settings.showCancelButton = true;

         SweetAlert.swal(this.settings,
           function(isConfirm){
             if (isConfirm) {
               confirmfns();
             }else {
               cancelfns();
             }
           }
         );
    };
 //  Mensaje de Defecto
    this.custonMessage = function () {
      SweetAlert.swal(this.settings);
    };
    // Mensaje Simple Ok
    this.sampleMessage = function (title,msg,state) {
        this.settings.title = $translate.instant('SWEAT_ALERTS.'+title);
        this.settings.text = $translate.instant('SWEAT_ALERTS.'+msg);
        this.settings.type =state;
        this.settings.showCancelButton= false;
        this.settings.confirmButtonText= $translate.instant('SWEAT_ALERTS.BTN_OK');
        this.settings.confirmButtonColor =$rootScope.$company.primary_color;
        this.custonMessage();
    };
    this.success = function(title,text) {
      this.settings.type  = "success";
      this.settings.title = title;
      this.settings.text  = text;
      this.settings.showCancelButton  = false;
      this.settings.confirmButtonText = $translate.instant('SWEAT_ALERTS.BTN_OK');
      this.custonMessage();
    };

    this.error = function(title,text) {
      this.settings.type  = "error";
      this.settings.title = title;
      this.settings.text  = text;
      this.settings.showCancelButton  = false;
      this.settings.confirmButtonText = $translate.instant('SWEAT_ALERTS.BTN_OK');
      this.custonMessage();
    };
    this.error400 = function(title,errors) {
      var msg="<ul>";
      this.settings.type  = "error";
      this.settings.title = title;
      this.settings.showCancelButton  = false;
      this.settings.confirmButtonText = $translate.instant('SWEAT_ALERTS.BTN_OK');
      this.settings.html=true;
        angular.forEach(errors, function(value, key){
          msg=msg+"<li class=text-justify>"+value+"</li>"
        });
        msg=msg+"</ul>";
      this.settings.text = msg;
      this.custonMessage();
    };

    this.warning = function(title,text) {
      this.settings.type="warning";
      this.settings.title=title;
      this.settings.text=text;
      this.settings.showCancelButton= false;
      this.settings.confirmButtonText= $translate.instant('SWEAT_ALERTS.BTN_OK');
      this.custonMessage();
    };

    this.info = function(title,text) {
      this.settings.type="info";
      this.settings.title=title;
      this.settings.text=text;
      this.settings.showCancelButton= false;
      this.settings.confirmButtonText= $translate.instant('SWEAT_ALERTS.BTN_OK');
      this.custonMessage();
};
    this.notify_error = function (error,clase) {
      var message=$translate.instant(error);
      notify({
            message: message,
            classes: clase,
            templateUrl:"views/formly/notify.html"
        });
    };


  }]);
})();
