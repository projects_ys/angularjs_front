(function(){
  'use strict';
  angular.module('Interactive')
  .factory('Request', ['HRAPI_CONF','$resource',function (HRAPI_CONF,$resource) {
    /**
     * @parameters
     * state: pending | managed
     * page: registro de arranque(opcional)
     *
     * @example
     * ?state=pending&approver=first&numrecords=10&record=0&search=NELSON+BELTRAN
     **/
    var url = HRAPI_CONF.apiBaseUrl('/requests_employees/:id.json');
    return $resource(url, { id: '@id' },{
      'save_formData': {
        method: "POST",
        transformRequest: angular.identity,
        headers: { 'Content-Type': undefined }
      }
    });

  }]);
})();
