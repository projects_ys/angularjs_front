(function(){
  'use strict';
  angular.module('Interactive')
  .factory('companyData', ['HRAPI_CONF','$http','$q',function (HRAPI_CONF,$http,$q) {
    var url = HRAPI_CONF.apiBaseUrl('/companies/info.json');

    return $http({method: 'GET', url:url})
    .then(function(response) {
      if (typeof response.data === 'object') {
        return response.data;
      } else {
        // invalid response
        return $q.reject(response.data);
      }
    }, function(response) {
      // something went wrong
      return $q.reject(response.data);
    });
  }])
})();