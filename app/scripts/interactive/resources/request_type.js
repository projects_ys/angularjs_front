(function(){
  'use strict';
  angular.module('Interactive')
  .factory('RequestType', ['$rootScope','$http','$q',function ($rootScope,$http,$q) {
    var url = $rootScope.url_request('/types_request/s.json');

    return $http({method: 'GET', url: url})
    .then(function(response,status) {
      if (typeof response.data === 'object') {
        if(response.data !== null){
          return response.data;
        }else{
          return {types_request:null};
        }
      } else {
        return $q.reject(response.data);
      }
    },function(response,status){
      return $q.reject(response.data);
    });
  }]);
})();
