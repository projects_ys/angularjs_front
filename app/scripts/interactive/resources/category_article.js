(function(){
  'use strict';
  angular.module('Interactive')
  .factory('CategoryArticle', ['HRAPI_CONF','$http','$q',function (HRAPI_CONF,$http,$q) {
    var url = HRAPI_CONF.apiBaseUrl('/articles/categories.json');
    return {
      get:function () {
        var deferred = $q.defer();
        $http({method: 'GET', url:url}).success(function(data) {
          deferred.resolve(data.categories_articles);
        }).error(function(msg, code) {
          deferred.reject(msg);
        });
        return deferred.promise;
      }
    }
  }])
})();
