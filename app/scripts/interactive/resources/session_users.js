(function(){
	'use strict';
	angular.module('Interactive')
	.factory('sessionUsers', [function () {
		var storage_name = 'data-login-user';
		var userTemp     = [];
		
		if(amplify.store(storage_name)) {
    	userTemp=amplify.store(storage_name);
		}else{
		  userTemp=[];
		}

		var userLogins = {
			getUser: function () {
    		if (amplify.store(storage_name)) {
    			amplify.store(storage_name);
      		userTemp = angular.fromJson(userTemp);
    		}
        return userTemp;
			},
			addUser: function (newUser) {	
				amplify.store(storage_name, newUser);
				userTemp = amplify.store(storage_name);
				
				userTemp = angular.fromJson(userTemp);
				return userTemp;
			},
			clearUser : function (user) {
				var users     = amplify.store(storage_name);
				var id_delete = users.employee_id;
				var temp      = new Object();

				for ( var u in users) {  
					if(id_delete != users['employee_id']){
						temp.push(users);
					}
				}
				amplify.store(storage_name, temp)
				return amplify.store(storage_name);
			}
		};
		return userLogins;
	}])
})();