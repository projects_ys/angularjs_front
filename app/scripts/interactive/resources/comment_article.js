(function(){
  'use strict';
  angular.module('Interactive')
    .factory('CommentArticle', ['HRAPI_CONF','$resource','$q',function (HRAPI_CONF,$resource,$q) {
    	var url = HRAPI_CONF.apiBaseUrl('/comments_articles/:id.json');
    	return $resource(url,{ id: '@id' },{
    		article_comments:{url:HRAPI_CONF.apiBaseUrl('/comments_articles.json?articleId=:articleId'),method:'GET',params: {articleId: '@articleId'}},
    	});
	}])
})();
