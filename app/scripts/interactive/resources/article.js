(function(){
  'use strict';
  angular.module('Interactive')
    .factory('Article', ['HRAPI_CONF','$resource','$q',function (HRAPI_CONF,$resource,$q) {
    	var url = HRAPI_CONF.apiBaseUrl('/articles/:id.json');
    	return $resource(url, { id: '@id' },{
    		'update':{method: 'PUT'},
        	'viewed':{ url:HRAPI_CONF.apiBaseUrl('/articles/:id/viewed '),method: 'PUT'}
    	});
	}])
})();
