(function(){
  'use strict';
  angular.module('Interactive')
  .factory('favoriteEmployees', ['HRAPI_CONF','$resource',function (HRAPI_CONF,$resource) {
    /**
     * @parameters
     * page   => numero de pagina (opcional)
     * search => valor a buscar (solo busca por nombre)
     * type   => favorites | nofavorites
     *
     * @example
     * ?type=favorites&page=1&search=NELSON+BELTRAN 
     **/
  	var url = HRAPI_CONF.apiBaseUrl('/favorite_employees/:id.json');
  	return $resource(url,null,{
  		limitResults:{url:HRAPI_CONF.apiBaseUrl('/favorite_employees/nofavorites/:record/:numrecords.json'),method:'GET',params:{record:'@record',numrecords:'@numrecords'}}
  	});
	}])
})();