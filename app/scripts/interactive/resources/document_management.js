(function(){
  'use strict';
  angular.module('Interactive')
  .factory('DocumentManagement', ['HRAPI_CONF',function (HRAPI_CONF) {
		/**
  	* @param int company_id
  	* @return Array
  	**/
  	var type_documents = function(company_id){
  		switch(company_id){
  			case 1: //harinera
          return [];
        case 2: //rcntv
          return [];
        case 4: //HR
          return [
            {id:1, name: 'Corporativos'},
            {id:2, name: 'Talento Humano'}
          ];
        case 3: //publicar
        case 6:
        case 10:
          return [
          	{id:1, name: 'Financieras'},
          	{id:2, name: 'Talento Humano'},
          	{id:3, name: 'Jurídica'}
          ];
        case 12: //odl
          return [];
  		}
  	}

  	/**
  	* @param int company_id
  	* @return Array
  	**/
  	var documents = function(company_id){
			switch(company_id) {
        case 1: //harinera
          return [];
        case 2: //rcntv
          return [];
        case 4: //HR
          return [
            {name:'Presentacion Corporativa 2016.pdf', url:'hrsolutions/Presentacion+Corporativa+2016.pdf', added:'Subido: Dic 20, 2016', type:'1', extension: 'pdf'},
            {name:'Presentación Mesa de Ayuda Envi.pdf', url:'hrsolutions/Presentaci%C3%B3n+Mesa+de+Ayuda+Envi.pdf', added:'Subido: Dic 20, 2016', type:'1', extension: 'pdf'},
            {name:'brochure_hrs.pdf', url:'hrsolutions/brochure_hrs.pdf', added:'Subido: Dic 20, 2016', type:'1', extension: 'pdf'},
            {name:'Resell SAP Learning Hub.pdf', url:'hrsolutions/Resell+SAP+Learning+Hub.pdf', added:'Subido: Dic 20, 2016', type:'1', extension: 'pdf'},
            {name:'Personalizacioìn de la plataforma hrinteractive.pdf', url:'hrsolutions/Personalizacio%C3%ACn+de+la+plataforma+hrinteractive.pdf', added:'Subido: Dic 20, 2016', type:'1', extension: 'pdf'},
            {name:'Customer Presentation - SAP Learning Hub Driving User Adoption Through Effective Learning.pdf', url:'hrsolutions/Customer+Presentation+-+SAP+Learning+Hub+Driving+User+Adoption+Through+Effective+Learning+(2).pdf', added:'Subido: Dic 20, 2016', type:'1', extension: 'pdf'},
            {name:'EDT.pdf', url:'hrsolutions/EDT.pdf', added:'Subido: Dic 20, 2016', type:'1', extension: 'pdf'},
            {name:'Presentacion Corporativa 2016.pptx', url:'hrsolutions/Presentacion+Corporativa+2016.pptx', added:'Subido: Dic 21, 2016', type:'2', extension: 'powerpoint'},
            {name:'Caracterizacion_gestion_del_talento_humano.pdf', url:'hrsolutions/caracterizacion_gestion_del_talento_humano.pdf', added:'Subido: Dic 21, 2016', type:'2', extension: 'pdf'}
          ];
        case 3: //publicar
        case 6:
        case 10:
          return [
            {name:'Política Gestion de Compras-mod.pdf', url:'publicar/Pol%C3%ADtica+Gestion+de+Compras-mod.pdf', added:'Subido: Nov 30, 2016', type:'1', extension: 'pdf'}
          ];
        case 12: //odl
          return [];
      }
  	}

  	return {
  		type_documents: type_documents,
  		documents: documents
  	};
	}]);
})();
