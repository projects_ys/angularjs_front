(function(){
  'use strict';
  angular.module('Interactive')
  .factory('Employee', ['$rootScope','$resource',function ($rootScope,$resource) {
  	var url = $rootScope.url_request('/employees');
  	return $resource(url, {},{
  		'autoservice_files':{url: $rootScope.url_request('/employees/autoservice_files'), method: 'GET'}
  	});
	}]);
})();
