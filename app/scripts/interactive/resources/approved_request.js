(function(){
  'use strict';
  angular.module('Interactive')
  .factory('ApprovedRequest', ['HRAPI_CONF','$resource',function (HRAPI_CONF,$resource) {
    /**
     * @parameters
     * state: pending | managed
     * approver: first | second
     * page: numero de pagina (opcional)
     * search = valor a buscar
     *
     * @example
     * ?state=pending&approver=first&numrecords=10&record=0&search=NELSON+BELTRAN 
     **/
    //var url = HRAPI_CONF.apiBaseUrl('/approvals');
    var url = HRAPI_CONF.apiBaseUrl('/approvals/:id.json');
    return $resource(url, { id: '@id' },{});
  }]);
})();
